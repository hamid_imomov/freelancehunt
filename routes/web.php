<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProjectController@home');
Route::get('/api/page', 'ApiController@apiPage');
Route::get('/api', 'ApiController@getApi');
Route::get('/projects/{category}', 'ProjectController@index');
Route::get('/projects/detail/{id}', 'ProjectController@detail');
Route::get('/stat', 'ProjectController@getStat');
Route::get('/filter', 'ProjectController@filter');

/*Route::get('/', function () {
    return view('welcome');
});*/
