@extends('layouts.web')

@section('title', 'Статистика')

@section('content')

    <h1>Статистика</h1>

    <br>

    @if(count($data) > 0)
        @foreach($data as $item)
            <p><strong>{{ $item->name }}</strong> &mdash; {{ $item->total }} шт.</p>
        @endforeach
    @else
        <p>Нет данных</p>
    @endif

@endsection
