<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function home(){
        $project = new Project();
        $data = $project->getList('all');
        return view('web.projects', compact('data'));
    }

    public function index($category){
        $project = new Project();
        $data = $project->getList($category);

        //print_r($data);

        return view('web.projects', compact('data'));
    }

    public function detail($id){
        $project = new Project();
        $data = $project->getByID($id);

        //dd($data);

        return view('web.projects_detail', compact('data'));
    }

    public function filter()
    {
        $project = new Project();
        $data = $project->getDataGraphic();
        //print_r($data);
        return view('web.graphic', compact('data'));
    }

    public function getStat()
    {
        $project = new Project();
        $data = $project->getStatistics();
        //print_r($data);
        return view('web.stat', compact('data'));
    }

}
