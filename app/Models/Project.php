<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project extends Model
{
    protected $fillable = [
        'p_id',
        'p_type',
        'name',
        'description',
        'description_html',
        'budget_amount',
        'budget_currency',
        'budget_uah',
        'bid_count',
        'is_remote_job',
        'is_premium',
        'is_only_for_plus',
        'location',
        'safe_type',
        'is_personal',
        'freelancer',
        'tags',
        'updates',
        'published_at',
        'expired_at',
        'links_self_api',
        'links_self_web',
        'links_comments',
        'links_bids'
    ];

    public function employer()
    {
        return $this->hasOne('App\Models\EmployerProject', 'project_id');
    }

    public function skill(){
        return $this->hasMany('App\Models\Skill');
    }

    public function projectExists($id){
        return $this->where('p_id', $id)->exists();
    }

    public function getByID($id){
        return $this->where('p_id', $id)->get()->first();
    }

    public function getList($category){

        if($category == 'all'){

            // return $this->orderBy('p_id','desc')->paginate(10);

            return DB::table('projects')
                ->select('projects.*', 'employers.*')
                ->join('employer_projects', 'employer_projects.project_id', '=', 'projects.id')
                ->join('employers', 'employers.id', '=', 'employer_projects.employer_id')
                ->orderBy('projects.p_id', 'desc')
                ->paginate(10);

        }
        else{
            return DB::table('projects')
                ->select('projects.*', 'employers.*')
                ->join('skills', 'skills.project_id', '=', 'projects.id')
                ->join('employer_projects', 'employer_projects.project_id', '=', 'projects.id')
                ->join('employers', 'employers.id', '=', 'employer_projects.employer_id')
                ->where(['skills.skill_id' => $category])
                ->orderBy('projects.p_id', 'desc')
                ->paginate(10);
        }
    }

    public function saveProject($arr, $converter){
        if(count($arr['data']) > 0){
            foreach ($arr['data'] as $item){

                $project = $this->projectExists($item['id']);

                if(!$project){

                    // converter valyut
                    $sum = 0;
                    if(!empty($item['attributes']['budget'])){
                        switch ($item['attributes']['budget']['currency']){
                            case 'RUB':
                                    $sum = $item['attributes']['budget']['amount'] * $converter[2]['sale'];
                                break;
                            case 'UAH':
                                $sum = $item['attributes']['budget']['amount'];
                                break;
                            case 'USD':
                                $sum = $item['attributes']['budget']['amount'] * $converter[0]['sale'];
                                break;
                            case 'EUR':
                                $sum = $item['attributes']['budget']['amount'] * $converter[1]['sale'];
                                break;
                            default:
                                $sum = $item['attributes']['budget']['amount'];
                        }
                    }

                    // add project
                    $p = new Project();
                    $p->p_id = $item['id'];
                    $p->p_type = $item['type'];
                    $p->name = $item['attributes']['name'];
                    $p->description = $item['attributes']['description'];
                    $p->description_html = $item['attributes']['description_html'];
					$p->budget_amount = $item['attributes']['budget']['amount'];
					$p->budget_currency = $item['attributes']['budget']['currency'];
                    $p->budget_uah = $sum;
                    $p->bid_count = $item['attributes']['bid_count'];
                    $p->is_remote_job = ($item['attributes']['is_remote_job'] == 'true') ? 1 : 0;
                    $p->is_premium = ($item['attributes']['is_premium'] == 'true') ? 1 : 0;
                    $p->is_only_for_plus = ($item['attributes']['is_only_for_plus'] == 'true') ? 1 : 0;
                    $p->location = $item['attributes']['location'];
                    $p->safe_type = $item['attributes']['safe_type'];
                    $p->is_personal = ($item['attributes']['is_personal'] == 'true') ? 1 : 0;
                    $p->freelancer = $item['attributes']['freelancer'];
                    $p->p_updates = '';     // getting empty values
                    $p->published_at = date('Y-m-d H:i:s', strtotime($item['attributes']['published_at']));
                    $p->expired_at = date('Y-m-d H:i:s', strtotime($item['attributes']['expired_at']));
                    $p->links_self_api = $item['links']['self']['api'];
                    $p->links_self_web = $item['links']['self']['web'];
                    $p->links_comments = $item['links']['comments'];
                    $p->links_bids = $item['links']['bids'];
                    $res = $p->save();

                    if($res){
                        // add skills
                        if(!empty($item['attributes']['skills'])){
                            foreach($item['attributes']['skills'] as $skill){
                                $s = new Skill();
                                $s->project_id = $p->id;
                                $s->skill_id = $skill['id'];
                                $s->name = $skill['name'];
                                $s->save();
                            }
                        }

                        // add status
                        if(!empty($item['attributes']['status'])){
                            $sts = new Status();
                            $sts->project_id = $p->id;
                            $sts->status_id = $item['attributes']['status']['id'];
                            $sts->name = $item['attributes']['status']['name'];
                            $sts->save();
                        }

                        // add tag
                        if(!empty($item['attributes']['tags'])){
                            foreach($item['attributes']['tags'] as $tag){
                                $t = new Tag();
                                $t->project_id = $p->id;
                                $t->tag_id = $tag['id'];
                                $t->name = $tag['name'];
                                $t->save();
                            }
                        }

                        // add employer
                        if(!empty($item['attributes']['employer'])){
                            $emp = new Employer();
                            $emp->emp_id = $item['attributes']['employer']['id'];
                            $emp->emp_type = $item['attributes']['employer']['type'];
                            $emp->login = $item['attributes']['employer']['login'];
                            $emp->first_name = $item['attributes']['employer']['first_name'];
                            $emp->last_name = $item['attributes']['employer']['last_name'];
                            $emp->avatar_small_url = $item['attributes']['employer']['avatar']['small']['url'];
                            $emp->avatar_large_url = $item['attributes']['employer']['avatar']['large']['url'];
                            $emp->self_url = $item['attributes']['employer']['self'];
                            $result = $emp->save();

                            // add epmplayer project
                            if($result){
                                $ep = new EmployerProject();
                                $ep->employer_id = $emp->id;
                                $ep->project_id = $p->id;
                                $ep->save();
                            }
                        }

                    }
                }

            }
        }
    }

    public function converter()
    {
        $url = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        $output = curl_exec($ch);
        curl_close($ch);

        $arr = json_decode($output, true);

        /*echo "<pre>";
        print_r($arr);
        echo "</pre>";*/

        return $arr;
    }

    public function getDataGraphic()
    {
        return [
            'до 500 грн' => $this->whereBetween('budget_uah', [1, 500])->count(),
            '500-1000 грн' => $this->whereBetween('budget_uah', [501, 1000])->count(),
            '1000-5000 грн' => $this->whereBetween('budget_uah', [1001, 5000])->count(),
            '5000-10000 грн' => $this->whereBetween('budget_uah', [5001, 10000])->count(),
            'более 10000 грн' => $this->whereBetween('budget_uah', [10001, 100000000000])->count()
        ];
    }

    public function getStatistics()
    {
        return DB::table('skills')
            ->select('name', DB::raw('count(*) as total'))
            ->groupBy('name')
            ->get();
    }

}
