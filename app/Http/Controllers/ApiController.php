<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function apiPage(){
        return view('web.api_page');
    }

    public function getApi()
    {
        $url = "https://api.freelancehunt.com/v2/projects";
        $token = "efd6ac3ee21385e78ae9401eb02bd84d35047ab0";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $token]);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        $output = curl_exec($ch);
        curl_close($ch);

        $arr = json_decode($output, true);

        $project = new Project();

        // converter
        $converter = $project->converter();

        // add project
        $project->saveProject($arr, $converter);

        return redirect('/api/page')->with(['success_message' => 'Успешно добавлены проекты.']);

        /*echo "<pre>";
        print_r($arr);
        echo "</pre>";*/

    }


}
