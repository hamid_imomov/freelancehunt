<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    protected $fillable = [
        'emp_id',
        'emp_type',
        'login',
        'first_name',
        'last_name',
        'avatar_small_url',
        'avatar_large_url',
        'self_url'
    ];
}
