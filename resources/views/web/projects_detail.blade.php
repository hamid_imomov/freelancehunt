@extends('layouts.web')

@section('title', $data->name)

@section('content')

    <h1>{{ $data->name }}</h1>

    <br>

   @if(!empty($data->budget_amount))
        <p>
            <span style="color: #a9a9a9;">бюджет:</span> {{ $data->budget_amount }} {{ $data->budget_currency }}
        </p>
    @endif

    <p><span style="color: #a9a9a9;">заказчик:</span> {{ $data->employer->employee->first_name}} {{ $data->employer->employee->last_name }} ({{ $data->employer->employee->login }})

    <div>{!! $data->description_html !!}</div>

    <br>
    <p><a href="{{ url('/projects/all') }}">Все проекты</a></p>

@endsection
