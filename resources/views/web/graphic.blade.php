@extends('layouts.web')

@section('title', 'Список проектов')

@section('content')

    <h1>График с распределением всех проектов по бюджету</h1>

    <br>

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div id="chartContainer" style="width: 100%; height: 300px"></div>
        </div>
    </div>


@endsection

@section('scripts')
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<script type="text/javascript">

    @php
        $str = '';
        foreach($data as $key => $value){
            $str .= '{ label: "' . $key . '", y: ' . $value . ', legendText: "' . $key . '" },';
        }
    @endphp

    window.onload = function() {
        $("#chartContainer").CanvasJSChart({
            axisY: {
                //title: "Products in %"
            },
            legend :{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{label} <br/> {y} шт.",
                    indexLabel: "{y} шт.",
                    dataPoints: [
                        @php
                            echo $str;
                        @endphp
                    ]
                }
            ]
        });
    }
</script>
@endsection
