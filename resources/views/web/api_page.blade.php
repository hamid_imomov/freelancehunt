@extends('layouts.web')

@section('title', 'Список проектов')

@section('content')

    <h1>API</h1>

    <br>

    <div class="row">
        <div class="col-md-6">
            @include('includes.alert')
            <p><a class="btn btn-primary" href="{{ url('/api') }}">Сохранить проектов (apidocs.freelancehunt.com)</a></p>
        </div>
    </div>

@endsection
