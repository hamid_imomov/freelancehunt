@extends('layouts.web')

@section('title', 'Список проектов')

@section('content')

    <h1>Проекты</h1>

    <br>
    <br>

    <p>
        <a class="btn btn-primary" href="{{ url('/projects/all') }}">Все проекты</a>
        <a class="btn btn-primary" href="{{ url('/projects/99') }}">Веб-программирование</a>
        <a class="btn btn-primary" href="{{ url('/projects/1') }}">PHP</a>
        <a class="btn btn-primary" href="{{ url('/projects/86') }}">Базы данных</a>
    </p>

    <br>
    <br>

    @if(count($data) > 0)
        @foreach($data as $item)
            <div>
                <a href="{{ url('/projects/detail/' . $item->p_id) }}">{{ $item->name }}</a>
                @if(!empty($item->budget_amount))
                    <br>
                    <span style="color: #a9a9a9;">бюджет:</span> {{ $item->budget_amount }} {{ $item->budget_currency }}
                @endif


                <br>
                <span style="color: #a9a9a9;">заказчик:</span> {{ $item->first_name }} {{ $item->last_name }} ({{ $item->login }})
            </div>
            <hr>
        @endforeach
        <br>
        {!! $data->render() !!}
    @else
        <p>Нет данных</p>
    @endif

@endsection
