<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployerProject extends Model
{
    protected $fillable = [
        'employer_id',
        'project_id'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Models\Employer', 'employer_id');
    }

}
