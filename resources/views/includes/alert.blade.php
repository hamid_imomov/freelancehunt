@if(Session::has('success_message'))
	<div class="alert alert-success" role="alert">
	  {{session('success_message')}}
	</div>
@endif

@if(Session::has('error_message'))
	<div class="alert alert-danger" role="alert">
	  {{session('error_message')}}
	</div>
@endif
