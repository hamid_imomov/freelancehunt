-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 04 2020 г., 05:12
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `frlh_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_employers`
--

CREATE TABLE `frlh_employers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `emp_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_small_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_large_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `self_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_employers`
--

INSERT INTO `frlh_employers` (`id`, `emp_id`, `emp_type`, `login`, `first_name`, `last_name`, `avatar_small_url`, `avatar_large_url`, `self_url`, `created_at`, `updated_at`) VALUES
(1, 768074, 'employer', 'Bogdan9', 'Богдан', 'К.', 'https://content.freelancehunt.com/profile/photo/50/Bogdan9.png', 'https://content.freelancehunt.com/profile/photo/225/Bogdan9.png', 'https://api.freelancehunt.com/v2/employers/768074', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(2, 548372, 'employer', 'Cthin', 'Андрей', 'К.', 'https://content.freelancehunt.com/profile/photo/50/Cthin.png', 'https://content.freelancehunt.com/profile/photo/225/Cthin.png', 'https://api.freelancehunt.com/v2/employers/548372', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(3, 751664, 'employer', 'Inella1379', 'Inna', 'I.', 'https://content.freelancehunt.com/profile/photo/50/Inella1379.png', 'https://content.freelancehunt.com/profile/photo/225/Inella1379.png', 'https://api.freelancehunt.com/v2/employers/751664', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(4, 573389, 'employer', 'Bohdanmatveev', 'Bohdan', 'M.', 'https://content.freelancehunt.com/profile/photo/50/Bohdanmatveev.png', 'https://content.freelancehunt.com/profile/photo/225/Bohdanmatveev.png', 'https://api.freelancehunt.com/v2/employers/573389', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(5, 391436, 'employer', 'zhernosek12', 'Andrei', 'Z.', 'https://content.freelancehunt.com/profile/photo/50/zhernosek12.png', 'https://content.freelancehunt.com/profile/photo/225/zhernosek12.png', 'https://api.freelancehunt.com/v2/employers/391436', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(6, 4, 'employer', 'freelancehunt', 'Freelancehunt', '.', 'https://content.freelancehunt.com/profile/photo/50/freelancehunt.png', 'https://content.freelancehunt.com/profile/photo/225/freelancehunt.png', 'https://api.freelancehunt.com/v2/employers/4', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(7, 312357, 'employer', 'cycleuzb', 'Aleksey', 'A.', 'https://content.freelancehunt.com/profile/photo/50/cycleuzb.png', 'https://content.freelancehunt.com/profile/photo/225/cycleuzb.png', 'https://api.freelancehunt.com/v2/employers/312357', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(8, 770502, 'employer', 'info_mimos', 'Dessy', 'K.', 'https://content.freelancehunt.com/profile/photo/50/info_mimos.png', 'https://content.freelancehunt.com/profile/photo/225/info_mimos.png', 'https://api.freelancehunt.com/v2/employers/770502', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(9, 242005, 'employer', 'fedorukl', 'Леонид', 'Ф.', 'https://content.freelancehunt.com/profile/photo/50/fedorukl.png', 'https://content.freelancehunt.com/profile/photo/225/fedorukl.png', 'https://api.freelancehunt.com/v2/employers/242005', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(10, 770566, 'employer', 'nascarzx', 'Alexandr', 'N.', 'https://content.freelancehunt.com/profile/photo/50/nascarzx.png', 'https://content.freelancehunt.com/profile/photo/225/nascarzx.png', 'https://api.freelancehunt.com/v2/employers/770566', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(11, 155976, 'employer', 'ast90', 'Alexey', 'A.', 'https://content.freelancehunt.com/profile/photo/50/ast90.png', 'https://content.freelancehunt.com/profile/photo/225/ast90.png', 'https://api.freelancehunt.com/v2/employers/155976', '2020-03-03 13:07:00', '2020-03-03 13:07:00'),
(12, 762355, 'employer', 'Diana1202', 'Diana', 'O.', 'https://content.freelancehunt.com/profile/photo/50/Diana1202.png', 'https://content.freelancehunt.com/profile/photo/225/Diana1202.png', 'https://api.freelancehunt.com/v2/employers/762355', '2020-03-03 13:23:18', '2020-03-03 13:23:18'),
(13, 770602, 'employer', 'usavtoimport', 'юрий', 'в.', 'https://content.freelancehunt.com/profile/photo/50/usavtoimport.png', 'https://content.freelancehunt.com/profile/photo/225/usavtoimport.png', 'https://api.freelancehunt.com/v2/employers/770602', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(14, 770620, 'employer', 'yaroslavukr', 'Ярослав', 'К.', 'https://content.freelancehunt.com/profile/photo/50/yaroslavukr.png', 'https://content.freelancehunt.com/profile/photo/225/yaroslavukr.png', 'https://api.freelancehunt.com/v2/employers/770620', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(15, 274681, 'employer', 'contrust2016', 'Екатерина', 'М.', 'https://content.freelancehunt.com/profile/photo/50/contrust2016.png', 'https://content.freelancehunt.com/profile/photo/225/contrust2016.png', 'https://api.freelancehunt.com/v2/employers/274681', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(16, 170028, 'employer', 'alyka1402', 'Alyka', 'K.', 'https://content.freelancehunt.com/profile/photo/50/alyka1402.png', 'https://content.freelancehunt.com/profile/photo/225/alyka1402.png', 'https://api.freelancehunt.com/v2/employers/170028', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(17, 338462, 'employer', 'cr1t', 'Дмитрий', 'Р.', 'https://content.freelancehunt.com/profile/photo/50/cr1t.png', 'https://content.freelancehunt.com/profile/photo/225/cr1t.png', 'https://api.freelancehunt.com/v2/employers/338462', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(18, 283796, 'employer', 'gtsky', 'Евгений', 'Ш.', 'https://content.freelancehunt.com/profile/photo/50/gtsky.png', 'https://content.freelancehunt.com/profile/photo/225/gtsky.png', 'https://api.freelancehunt.com/v2/employers/283796', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(19, 155976, 'employer', 'ast90', 'Alexey', 'A.', 'https://content.freelancehunt.com/profile/photo/50/ast90.png', 'https://content.freelancehunt.com/profile/photo/225/ast90.png', 'https://api.freelancehunt.com/v2/employers/155976', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(20, 136136, 'employer', 'active-game', 'Karen', 'A.', 'https://content.freelancehunt.com/profile/photo/50/active-game.png', 'https://content.freelancehunt.com/profile/photo/225/active-game.png', 'https://api.freelancehunt.com/v2/employers/136136', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(21, 76918, 'employer', 'oleksiiii', 'АЛЕКСЕЙ', 'С.', 'https://content.freelancehunt.com/profile/photo/50/oleksiiii.png', 'https://content.freelancehunt.com/profile/photo/225/oleksiiii.png', 'https://api.freelancehunt.com/v2/employers/76918', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(22, 155976, 'employer', 'ast90', 'Alexey', 'A.', 'https://content.freelancehunt.com/profile/photo/50/ast90.png', 'https://content.freelancehunt.com/profile/photo/225/ast90.png', 'https://api.freelancehunt.com/v2/employers/155976', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(23, 770631, 'employer', 'arsens', 'Arsen', 'S.', 'https://content.freelancehunt.com/profile/photo/50/arsens.png', 'https://content.freelancehunt.com/profile/photo/225/arsens.png', 'https://api.freelancehunt.com/v2/employers/770631', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(24, 770060, 'employer', 'Kristina2011', 'Кристина', 'Б.', 'https://content.freelancehunt.com/profile/photo/50/Kristina2011.png', 'https://content.freelancehunt.com/profile/photo/225/Kristina2011.png', 'https://api.freelancehunt.com/v2/employers/770060', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(25, 770554, 'employer', 'alex202012', 'Александр', 'К.', 'https://content.freelancehunt.com/profile/photo/50/alex202012.png', 'https://content.freelancehunt.com/profile/photo/225/alex202012.png', 'https://api.freelancehunt.com/v2/employers/770554', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(26, 366362, 'employer', 'NikL', 'Николай', 'Б.', 'https://content.freelancehunt.com/profile/photo/50/NikL.png', 'https://content.freelancehunt.com/profile/photo/225/NikL.png', 'https://api.freelancehunt.com/v2/employers/366362', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(27, 512861, 'employer', 'familyboxvlog7', 'Яна', 'И.', 'https://content.freelancehunt.com/profile/photo/50/familyboxvlog7.png', 'https://content.freelancehunt.com/profile/photo/225/familyboxvlog7.png', 'https://api.freelancehunt.com/v2/employers/512861', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(28, 324730, 'employer', 'Parket_planet', 'Oleg', 'K.', 'https://content.freelancehunt.com/profile/photo/50/Parket_planet.png', 'https://content.freelancehunt.com/profile/photo/225/Parket_planet.png', 'https://api.freelancehunt.com/v2/employers/324730', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(29, 591804, 'employer', 'artvolokitin', 'Артем', 'В.', 'https://content.freelancehunt.com/profile/photo/50/artvolokitin.png', 'https://content.freelancehunt.com/profile/photo/225/artvolokitin.png', 'https://api.freelancehunt.com/v2/employers/591804', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(30, 770653, 'employer', 'CVstar0524', 'IvanKotov', 'C.', 'https://content.freelancehunt.com/profile/photo/50/CVstar0524.png', 'https://content.freelancehunt.com/profile/photo/225/CVstar0524.png', 'https://api.freelancehunt.com/v2/employers/770653', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(31, 770657, 'employer', 'elizabeth_vasilenko', 'Элизабет', 'В.', 'https://content.freelancehunt.com/profile/photo/50/elizabeth_vasilenko.png', 'https://content.freelancehunt.com/profile/photo/225/elizabeth_vasilenko.png', 'https://api.freelancehunt.com/v2/employers/770657', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(32, 108626, 'employer', 'Oleksandr__', 'Александр', 'Л.', 'https://content.freelancehunt.com/profile/photo/50/Oleksandr__.png', 'https://content.freelancehunt.com/profile/photo/225/Oleksandr__.png', 'https://api.freelancehunt.com/v2/employers/108626', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(33, 764839, 'employer', 'Elena62', 'Елена', 'П.', 'https://content.freelancehunt.com/profile/photo/50/Elena62.png', 'https://content.freelancehunt.com/profile/photo/225/Elena62.png', 'https://api.freelancehunt.com/v2/employers/764839', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(34, 770672, 'employer', 'TSafin', 'Timur', 'S.', 'https://content.freelancehunt.com/profile/photo/50/TSafin.png', 'https://content.freelancehunt.com/profile/photo/225/TSafin.png', 'https://api.freelancehunt.com/v2/employers/770672', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(35, 359857, 'employer', 'madlexkuzmenko', 'Николай', 'К.', 'https://content.freelancehunt.com/profile/photo/50/madlexkuzmenko.png', 'https://content.freelancehunt.com/profile/photo/225/madlexkuzmenko.png', 'https://api.freelancehunt.com/v2/employers/359857', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(36, 371500, 'employer', 'anton_gavrik', 'Антон', 'Г.', 'https://content.freelancehunt.com/profile/photo/50/anton_gavrik.png', 'https://content.freelancehunt.com/profile/photo/225/anton_gavrik.png', 'https://api.freelancehunt.com/v2/employers/371500', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(37, 283921, 'employer', 'ubiquito', 'Александр', 'В.', 'https://content.freelancehunt.com/profile/photo/50/ubiquito.png', 'https://content.freelancehunt.com/profile/photo/225/ubiquito.png', 'https://api.freelancehunt.com/v2/employers/283921', '2020-03-03 14:13:55', '2020-03-03 14:13:55'),
(38, 98681, 'employer', 'Tanitouch', 'Татьяна', 'А.', 'https://content.freelancehunt.com/profile/photo/50/Tanitouch.png', 'https://content.freelancehunt.com/profile/photo/225/Tanitouch.png', 'https://api.freelancehunt.com/v2/employers/98681', '2020-03-03 14:13:55', '2020-03-03 14:13:55');

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_employer_projects`
--

CREATE TABLE `frlh_employer_projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employer_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_employer_projects`
--

INSERT INTO `frlh_employer_projects` (`id`, `employer_id`, `project_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(2, 2, 2, '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(3, 3, 3, '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(4, 4, 4, '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(5, 5, 5, '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(6, 6, 6, '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(7, 7, 7, '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(8, 8, 8, '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(9, 9, 9, '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(10, 10, 10, '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(11, 11, 11, '2020-03-03 13:07:00', '2020-03-03 13:07:00'),
(12, 12, 12, '2020-03-03 13:23:18', '2020-03-03 13:23:18'),
(13, 13, 13, '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(14, 14, 14, '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(15, 15, 15, '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(16, 16, 16, '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(17, 17, 17, '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(18, 18, 18, '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(19, 19, 19, '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(20, 20, 20, '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(21, 21, 21, '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(22, 22, 22, '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(23, 23, 23, '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(24, 24, 24, '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(25, 25, 25, '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(26, 26, 26, '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(27, 27, 27, '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(28, 28, 28, '2020-03-03 13:46:57', '2020-03-03 13:46:57'),
(29, 29, 29, '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(30, 30, 30, '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(31, 31, 31, '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(32, 32, 32, '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(33, 33, 33, '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(34, 34, 34, '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(35, 35, 35, '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(36, 36, 36, '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(37, 37, 37, '2020-03-03 14:13:55', '2020-03-03 14:13:55'),
(38, 38, 38, '2020-03-03 14:13:55', '2020-03-03 14:13:55');

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_failed_jobs`
--

CREATE TABLE `frlh_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_migrations`
--

CREATE TABLE `frlh_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_migrations`
--

INSERT INTO `frlh_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_01_194006_create_projects_table', 1),
(5, '2020_03_01_194904_create_skills_table', 1),
(6, '2020_03_01_195036_create_statuses_table', 1),
(7, '2020_03_01_195156_create_employers_table', 1),
(8, '2020_03_02_051158_create_employer_projects_table', 2),
(9, '2020_03_02_060830_create_tags_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_password_resets`
--

CREATE TABLE `frlh_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_projects`
--

CREATE TABLE `frlh_projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` int(11) DEFAULT NULL,
  `p_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget_amount` decimal(12,2) DEFAULT NULL,
  `budget_currency` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget_uah` decimal(12,2) UNSIGNED DEFAULT NULL,
  `bid_count` int(11) DEFAULT NULL,
  `is_remote_job` tinyint(4) DEFAULT NULL,
  `is_premium` tinyint(4) DEFAULT NULL,
  `is_only_for_plus` tinyint(4) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safe_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_personal` tinyint(4) DEFAULT NULL,
  `freelancer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_updates` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `links_self_api` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `links_self_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `links_comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `links_bids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_projects`
--

INSERT INTO `frlh_projects` (`id`, `p_id`, `p_type`, `name`, `description`, `description_html`, `budget_amount`, `budget_currency`, `budget_uah`, `bid_count`, `is_remote_job`, `is_premium`, `is_only_for_plus`, `location`, `safe_type`, `is_personal`, `freelancer`, `p_tags`, `p_updates`, `published_at`, `expired_at`, `links_self_api`, `links_self_web`, `links_comments`, `links_bids`, `created_at`, `updated_at`) VALUES
(1, 641976, 'project', 'На фреймворке Е5 сделать отображение вещей в простой ммо игре', 'Техническое задание:\nВ лавке редкостей есть категории вещей. Нужно чтобы при нажатии на категорию вещей, например доспехи - выводились доспехи как показано на картинке(на первой картинке это то что уже есть). Если это оружие соответственно все доступное по уровню оружие.Все вещи и их типы хранятся в таблице Вещи. Название ссылки зависит от ее типа вещи. Если это доспехи то armor. В целом нужно вывести оружие и доспехи.Что касается css то отображение вещей можно сделать обычной таблицей.Более детально обсудим в лс. Сроки работы на ваше усмотрение.\nОплата на карту ПриватБанка. Предлагайте свою цену за выполнения данной работы. Вот сайт фреймворка Е5 для ознакомления с его структурой https://rucms.org/', '<p>Техническое задание:<br />В лавке редкостей есть категории вещей. Нужно чтобы при нажатии на категорию вещей, например доспехи - выводились доспехи как показано на картинке(на первой картинке это то что уже есть). Если это оружие соответственно все доступное по уровню оружие.Все вещи и их типы хранятся в таблице Вещи. Название ссылки зависит от ее типа вещи. Если это доспехи то armor. В целом нужно вывести оружие и доспехи.Что касается css то отображение вещей можно сделать обычной таблицей.Более детально обсудим в лс. Сроки работы на ваше усмотрение.<br />Оплата на карту ПриватБанка. Предлагайте свою цену за выполнения данной работы. Вот сайт фреймворка Е5 для ознакомления с его структурой <a rel=\"nofollow\" href=\"https://rucms.org/\" target=\"_blank\">https://rucms.org/</a></p>', NULL, NULL, '0.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:52:26', '2020-03-20 17:52:26', 'https://api.freelancehunt.com/v2/projects/641976', 'https://freelancehunt.com/project/na-freymvorke-e5-sdelat-otobrazhenie-veschey/641976.html', 'https://api.freelancehunt.com/v2/projects/641976/comments', 'https://api.freelancehunt.com/v2/projects/641976/bids', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(2, 641975, 'project', 'Курсовая по макроэкономикэ', 'Тема: Монетарные и немонетарные факторы инфляции. Инфляционные процессы и пути их преодоления в России.\nОбъём: 35 стр. \nАнтиплагиат (https://www.antiplagiat.ru/) 77%, etxt (https://www.etxt.ru/antiplagiat/) 42%\nСрок: неделя.\n\n\nРабота теоретическая, но со статистикой.', '<p>Тема: Монетарные и немонетарные факторы инфляции. Инфляционные процессы и пути их преодоления в России.</p><p>Объём: 35 стр.&nbsp;</p><p>Антиплагиат (https://www.antiplagiat.ru/) 77%, etxt (https://www.etxt.ru/antiplagiat/) 42%</p><p>Срок: неделя.</p><p><br /></p><p>Работа теоретическая, но со статистикой.</p><p><br /></p>', '1200.00', 'RUB', '456.00', 2, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:50:59', '2020-03-10 17:50:59', 'https://api.freelancehunt.com/v2/projects/641975', 'https://freelancehunt.com/project/kursovaya-po-makroekonomike/641975.html', 'https://api.freelancehunt.com/v2/projects/641975/comments', 'https://api.freelancehunt.com/v2/projects/641975/bids', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(3, 641974, 'project', 'Нужен монтажер видео роликов', 'Детский молодой ютюб канал ищет монтажера.\nМонтажер должен уметь: находить актуальные картинки, футажи, видео-фрагменты, накладывать различные эффекты, вставлять смешные фрагменты разбавлять контент.\nВидео длиной от 7-10 минут.', '<p>Детский молодой ютюб канал ищет монтажера.<br />Монтажер должен уметь: находить актуальные картинки, футажи, видео-фрагменты, накладывать различные эффекты, вставлять смешные фрагменты разбавлять контент.<br />Видео длиной от 7-10 минут.</p>', NULL, NULL, '0.00', 4, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:48:02', '2020-03-10 17:48:02', 'https://api.freelancehunt.com/v2/projects/641974', 'https://freelancehunt.com/project/nuzhen-montazher-video-rolikov/641974.html', 'https://api.freelancehunt.com/v2/projects/641974/comments', 'https://api.freelancehunt.com/v2/projects/641974/bids', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(4, 641969, 'project', 'Описание карточек товара, написание статей стоматологической тематики', 'Требуется копирайтер для написания уникальных текстов на стоматологическую тематику (целевая аудитория стоматологи). Рассматривается сотрудничество на регулярной основе. Сейчас стоит задача описать большой объем карточек товара для интернет-магазина https://amelmedical.com.ua/ru/: стоматологические инструменты, стоматологическое оборудование, материалы.', '<p>Требуется копирайтер для написания уникальных текстов на стоматологическую тематику (целевая аудитория стоматологи). Рассматривается сотрудничество на регулярной основе. Сейчас стоит задача описать большой объем карточек товара для интернет-магазина <a data-fr-linked=\"true\" href=\"https://amelmedical.com.ua/ru/:\" rel=\"noopener noreferrer noopener noreferrer nofollow\" target=\"_blank\">https://amelmedical.com.ua/ru/:</a> стоматологические инструменты, стоматологическое оборудование, материалы.</p>', NULL, NULL, '0.00', 10, 1, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:41:01', '2020-03-10 17:41:01', 'https://api.freelancehunt.com/v2/projects/641969', 'https://freelancehunt.com/project/opisanie-kartochek-tovara-napisanie-statey-stomatologicheskoy/641969.html', 'https://api.freelancehunt.com/v2/projects/641969/comments', 'https://api.freelancehunt.com/v2/projects/641969/bids', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(5, 641966, 'project', 'Верстка 2 страниц HTML', 'Необходимо реализовать верстку 2 страниц, далее шаблон будет натягиваться на движок Wordpress\nДля работы есть исходники в .PSD\nСтраницы которые необходимо сверстать:\n- Главная\n- Карточка товара\n\nНа страницах должен быть адаптив, и желательно минимальная анимация, на кнопках и формах', '<p><br /></p><p>Необходимо реализовать верстку 2 страниц, далее шаблон будет натягиваться на движок Wordpress<br />Для работы есть исходники в .PSD</p><p>Страницы которые необходимо сверстать:</p><p>- Главная<br />- Карточка товара</p><p><br />На страницах должен быть адаптив, и желательно минимальная анимация, на кнопках и формах</p><p><br /></p>', '1000.00', 'UAH', '1000.00', 34, 0, 0, 0, NULL, 'split', 0, NULL, NULL, '', '2020-03-03 17:36:45', '2020-03-05 17:36:45', 'https://api.freelancehunt.com/v2/projects/641966', 'https://freelancehunt.com/project/verstka-stranits-html/641966.html', 'https://api.freelancehunt.com/v2/projects/641966/comments', 'https://api.freelancehunt.com/v2/projects/641966/bids', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(6, 625047, 'project', 'SEO-специалист для долгосрочного сотрудничества', 'Для оптимизации и поискового продвижения freelancehunt.com требуется SEO-специалист.\nРассматриваем исключительно постоянное долгосрочное сотрудничество.Постоянные работы:Рекомендации по SEO-оптимизации и внутренней структуре контента.Выработка, ведение и коррекция стратегии по продвижению ресурса в поисковых системах.Мероприятия по работе с внешними факторами ранжирования.Настройка отслеживания целей.С кем нам будет комфортно работать:опыт работы от года, портфолио результативных проектов.инициативность, ответственность, самостоятельность.желание сделать фриланс более популярным, особенно среди заказчиков.Пожалуйста, в ставке укажите, насколько вы соответствуете требованиям и опишите ваше предложение - примерный план продвижения и распределение бюджета. Ставки скрыты, чтобы вы могли максимально все расписать.Обратите внимание: для нас важно, чтобы ваша компетенция распространялась, как на Google, так и на Yandex.', '<p>Для оптимизации и поискового продвижения <a href=\"http://freelancehunt.com/\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">freelancehunt.com</a> требуется SEO-специалист.</p><p><strong>Рассматриваем исключительно постоянное долгосрочное сотрудничество.</strong></p><hr /><p><em>Постоянные работы:</em></p><ol><li>Рекомендации по SEO-оптимизации и внутренней структуре контента.</li><li><strong>Выработка, ведение и коррекция стратегии по продвижению ресурса в поисковых системах.</strong></li><li>Мероприятия по работе с внешними факторами ранжирования.</li><li>Настройка отслеживания целей.</li></ol><hr /><p><em>С кем нам будет комфортно работать</em><em>:</em></p><ul><li>опыт работы от года, портфолио результативных проектов.</li><li><strong>инициативность, ответственность, самостоятельность.</strong></li><li>желание сделать фриланс более популярным, особенно среди заказчиков.</li></ul><hr /><p>Пожалуйста, в ставке укажите, насколько вы соответствуете требованиям и <strong>о</strong><strong>пишите ваше предложение - примерный план продвижения и распределение бюджета</strong>. Ставки скрыты, чтобы вы могли максимально все расписать.</p><hr /><p>Обратите внимание: для нас важно, чтобы ваша компетенция распространялась, как на Google, так и на Yandex.</p>', NULL, NULL, '0.00', 43, 0, 1, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:36:04', '2020-03-08 17:26:36', 'https://api.freelancehunt.com/v2/projects/625047', 'https://freelancehunt.com/project/seo-spetsialist-dlya-dolgosrochnogo-sotrudnichestva/625047.html', 'https://api.freelancehunt.com/v2/projects/625047/comments', 'https://api.freelancehunt.com/v2/projects/625047/bids', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(7, 641964, 'project', 'Вывести таблицу в Товар prestashop', 'Добрый день \n\n\nНам нужно в Админке на Страничке товара вывести таблицу с возможностью сохранения в базу\nБаза готова столбцы готовы \n\n\nОт вас требуется создатьСоздать Закладку SKU в Карточке товара Вывести таблицу из базыПроверить способность сохранения в базу при добавлении SKU Чтобы устанавливался через модули\n\nЕсть пример работы\n\n\nЖдем ваших предложений', '<p>Добрый день&nbsp;</p><p><br /></p><p>Нам нужно в Админке на Страничке товара вывести таблицу с возможностью сохранения в базу</p><p>База готова столбцы готовы&nbsp;</p><p><br /></p><p>От вас требуется создать</p><ol><li>Создать Закладку SKU в Карточке товара&nbsp;</li><li>Вывести таблицу из базы</li><li>Проверить способность сохранения в базу при добавлении SKU&nbsp;</li><li>Чтобы устанавливался через модули</li></ol><p><br /></p><p>Есть пример работы</p><p><br /></p><p>Ждем ваших предложений</p>', '1000.00', 'UAH', '1000.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:34:43', '2020-03-10 17:34:43', 'https://api.freelancehunt.com/v2/projects/641964', 'https://freelancehunt.com/project/vyivesti-tablitsu-tovar-prestashop/641964.html', 'https://api.freelancehunt.com/v2/projects/641964/comments', 'https://api.freelancehunt.com/v2/projects/641964/bids', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(8, 641963, 'project', 'Доработка страницы товаров', 'Мне нуен специалист,\nНа сегодняшний день, у меня есть, который нормально работает, однако, сейчас возникло необходимость найти специалиста для выполнения следующих задании:-Изменение дизайна страницы продуктов;Сопровождение моего сайта;Важная информация:-Дизайн моего сайта - Opencart - 2.1, плюс есть собственный шаблон (который основан на значении по умолчанию, но некоторые страницы были сильно изменены);Дальнейшие изменение только через Opencart - 2.1На сайте используется множество плагинов / расширений, включая модификации ocmod, и поэтому лучше быть особенно осторожным при внесении новых изменений;Знание английский язык для чтения почти обязательно;Желательно, долгосрочное сотрудничество – я сотрудничал с человеком, который вёл мой сайт более 3 года;Условие оплаты и способы оплаты договорные;Хорошо иметь базовое знание SEO UX/UI, однако, не обязательно;', '<p>Мне нуен специалист,</p><p>На сегодняшний день, у меня есть, который нормально работает, однако, сейчас возникло необходимость найти специалиста для выполнения следующих задании:-</p><ul><li>Изменение дизайна страницы продуктов;</li><li>Сопровождение моего сайта;</li></ul><p>Важная информация:-</p><ul><li>Дизайн моего сайта - Opencart - 2.1, плюс есть собственный шаблон (который основан на значении по умолчанию, но некоторые страницы были сильно изменены);</li><li>Дальнейшие изменение только через Opencart - 2.1</li><li>На сайте используется множество плагинов / расширений, включая модификации ocmod, и поэтому лучше быть особенно осторожным при внесении новых изменений;</li><li>Знание английский язык для чтения почти обязательно;</li><li>Желательно, долгосрочное сотрудничество &ndash; я сотрудничал с человеком, который вёл мой сайт более 3 года;</li><li>Условие оплаты и способы оплаты договорные;</li><li>Хорошо иметь базовое знание SEO UX/UI, однако, не обязательно;</li></ul>', NULL, NULL, '0.00', 1, 1, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:33:20', '2020-03-18 17:33:20', 'https://api.freelancehunt.com/v2/projects/641963', 'https://freelancehunt.com/project/dorabotka-stranitsyi-tovarov/641963.html', 'https://api.freelancehunt.com/v2/projects/641963/comments', 'https://api.freelancehunt.com/v2/projects/641963/bids', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(9, 641961, 'project', 'Сделать счетчик на конечных стр. сайт на PHP', 'Сделать счетчик на конечных стр. сайт на PHP самописец. ТЗ прилагается в ПДФ файле. Счетчик должен вписываться в дизайн сайта, приходится дописывать символы, чтоб https://freelancehunt.com/  мог пропустить работу.', '<p>Сделать счетчик на конечных стр. сайт на PHP самописец. ТЗ прилагается в ПДФ файле. Счетчик должен вписываться в дизайн сайта, приходится дописывать символы, чтоб&nbsp;<a rel=\"nofollow\" href=\"https://freelancehunt.com/\" target=\"_blank\">https://freelancehunt.com/</a>&nbsp; мог пропустить работу.</p>', NULL, NULL, '0.00', 2, 0, 0, 0, NULL, 'split', 0, NULL, NULL, '', '2020-03-03 17:31:23', '2020-03-10 17:31:23', 'https://api.freelancehunt.com/v2/projects/641961', 'https://freelancehunt.com/project/sdelat-schetchik-na-konechnyih-str-sayt/641961.html', 'https://api.freelancehunt.com/v2/projects/641961/comments', 'https://api.freelancehunt.com/v2/projects/641961/bids', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(10, 641957, 'project', 'Доработка сайта на WordPress', 'Нужен опытный программист по WordPress который выполнит стек с небольшим количеством задач. Задачи как и средней сложности так и высокой.\nЗадачи нужно выполнять по приоритету (в трело выставили приоритет)\nГлавная цель сайта - максимально автоматизированный сайт сателлит для быстрого развертывания и автоматического наполнения новыми товарами. Нужно будет создать 3 копии (установщика) на разных языках, а так же автоматизировать некоторые функции стандартного шаблона.\n\nC задачами можно ознакомится здесь https://trello.com/b/h53Xdi6D/imperiastore-wp', '<p>Нужен опытный программист по WordPress который выполнит стек с небольшим количеством задач. Задачи как и средней сложности так и высокой.<br />Задачи нужно выполнять по приоритету (в трело выставили приоритет)<br />Главная цель сайта - максимально автоматизированный сайт сателлит для быстрого развертывания и автоматического наполнения новыми товарами. Нужно будет создать 3 копии (установщика) на разных языках, а так же автоматизировать некоторые функции стандартного шаблона.<br /><br />C задачами можно ознакомится здесь <a href=\"https://trello.com/b/h53Xdi6D/imperiastore-wp\" rel=\"noopener noreferrer nofollow\" target=\"_blank\">https://trello.com/b/h53Xdi6D/imperiastore-wp </a></p>', '5000.00', 'UAH', '5000.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 17:28:16', '2020-03-06 17:28:16', 'https://api.freelancehunt.com/v2/projects/641957', 'https://freelancehunt.com/project/dorabotka-sayta-na-wordpress/641957.html', 'https://api.freelancehunt.com/v2/projects/641957/comments', 'https://api.freelancehunt.com/v2/projects/641957/bids', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(11, 641978, 'project', 'Установить модуль техдок для опенкара 3.2.2. или 3.2.5', 'Установить модуль техдок для опенкара 3.2.2. или 3.2.5\n\n\n\n\n\n\n....................\n....................\n....................\n....................\n..............', '<p>Установить модуль техдок для опенкара 3.2.2. или 3.2.5</p><p><br /></p><p><br /></p><p><br /></p><p>....................</p><p>....................</p><p>....................</p><p>....................</p><p>..............</p>', '500.00', 'UAH', '500.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:05:29', '2020-03-10 18:05:29', 'https://api.freelancehunt.com/v2/projects/641978', 'https://freelancehunt.com/project/ustanovit-modul-tehdok-dlya-openkara-322/641978.html', 'https://api.freelancehunt.com/v2/projects/641978/comments', 'https://api.freelancehunt.com/v2/projects/641978/bids', '2020-03-03 13:06:59', '2020-03-03 13:06:59'),
(12, 641988, 'project', 'Создать сайт', 'Нужно создать сайт для салона красоты :\n\n\n•в формате Word Press \n•с установкой  на ваш хостинг \n•подключением настроек \n• базовая SEO оптимизация \n• оптимизация скорости загрузки\n\n\nБюджет $100\n\n\nПрикрепляйте \n•примеры ваших работ которые отвечают требованиям указанным выше\n• сроки выполнения задачи ', '<p>Нужно создать сайт для салона красоты :</p><p><br /></p><p>&bull;в формате Word Press&nbsp;</p><p>&bull;с установкой &nbsp;на ваш хостинг&nbsp;</p><p>&bull;подключением настроек&nbsp;</p><p>&bull; базовая SEO оптимизация&nbsp;</p><p>&bull; оптимизация скорости загрузки</p><p><br /></p><p>Бюджет $100</p><p><br /></p><p>Прикрепляйте&nbsp;</p><p>&bull;примеры ваших работ которые отвечают требованиям указанным выше</p><p>&bull; сроки выполнения задачи&nbsp;</p><p><br /></p><p><br /></p>', '2500.00', 'UAH', '2500.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:22:33', '2020-03-10 18:22:33', 'https://api.freelancehunt.com/v2/projects/641988', 'https://freelancehunt.com/project/sozdat-sayt/641988.html', 'https://api.freelancehunt.com/v2/projects/641988/comments', 'https://api.freelancehunt.com/v2/projects/641988/bids', '2020-03-03 13:23:16', '2020-03-03 13:23:16'),
(13, 641987, 'project', 'Разработка дизайна для компании', 'Доброго. Наша компания занимается электромобилями и авто из США. У нас уже есть сайт, но нам нужны решения для банеров по улицах, дизайн логотипа, автомобильных наклеек, рамок для фото в Инстаграм итд.\nВ идеале разработать фирменный стиль.\n\nВ первую очередь надо логотип, уже после этого будем работать дальше.\nУже логотип есть, возможно Вы сможете что-то с ним сделать.', '<p>Доброго. Наша компания занимается электромобилями и авто из США. У нас уже есть сайт, но нам нужны решения для банеров по улицах, дизайн логотипа, автомобильных наклеек, рамок для фото в Инстаграм итд.<br />В идеале разработать фирменный стиль.<br /><br />В первую очередь надо логотип, уже после этого будем работать дальше.<br />Уже логотип есть, возможно Вы сможете что-то с ним сделать.</p>', '1000.00', 'UAH', '1000.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:21:49', '2020-03-10 18:21:49', 'https://api.freelancehunt.com/v2/projects/641987', 'https://freelancehunt.com/project/razrabotka-dizayna-dlya-kompanii/641987.html', 'https://api.freelancehunt.com/v2/projects/641987/comments', 'https://api.freelancehunt.com/v2/projects/641987/bids', '2020-03-03 13:23:18', '2020-03-03 13:23:18'),
(14, 641986, 'project', 'Разработка логотипа', 'Всем привет!\n\n\nНужен логотип для сайта по продаже автозапчастей.  Адрес сайта: https://rservise.com.ua/ . Обязательное условие присутствие название сайта в лого (RServise). Пожалуйста предлагайте свои варианты и цены.', '<p>Всем привет!</p><p><br /></p><p>Нужен логотип для сайта по продаже автозапчастей. &nbsp;Адрес сайта: <a rel=\"nofollow\" href=\"https://rservise.com.ua/\" target=\"_blank\">https://rservise.com.ua/</a> . Обязательное условие присутствие название сайта в лого (RServise). Пожалуйста предлагайте свои варианты и цены.</p><p><br /></p><p><br /></p>', NULL, NULL, '0.00', 6, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:20:59', '2020-03-06 18:20:59', 'https://api.freelancehunt.com/v2/projects/641986', 'https://freelancehunt.com/project/razrabotka-logotipa/641986.html', 'https://api.freelancehunt.com/v2/projects/641986/comments', 'https://api.freelancehunt.com/v2/projects/641986/bids', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(15, 641985, 'project', 'Настройка амоСРМ', 'Задача № 1. Нужно настроить передачу с 1 сайта + 2х лендингов в АМО UTM МЕТКОК \nДля этого нужно:\n 1. Настроить на сайте передачу UTM-метки в письмо \n2. В рекламных обьявлениях контекстолог добавляет метку в каждый ключ. В источниках на наш сайт должны быть ссылки с ютм метками допустим для рекламы в вк ссылка такого вида: utm_source=vkontakte&utm_medium=cpc&utm_campaign={campaign_id}&utm_content={ad_id} \nЭтот ресурс генерирует метки: https://tilda.cc/ru/utm/ \n3. Настроить сохранение ютм меток в кэше браузера, на тот случай если пользователь перейдет на другую страницу сайта и с нее оформит заявку \n4. передача ютм меток в создаваемую сделку в амосрм в поля, которые есть в карточке сделки Что должно отображаться в карточке сделки по UTM-меткам: -utm_source - Источник кампании -utm_medium - Тип трафика -utm_campaign - Название кампании -utm_content - Идентификатор объявления -utm_term - Ключевое слово \nЗадача № 2. На сайте и на лендинга 2 шт будут добавлены в шапку новые формы обратной связи и в форме будет возможность прикрепить файл, нужно будет эти формы соединить с АМО, чтобы заявки, отправленные с этих форм попадали в воронку НОВЫЕ ЗАЯВКИ в виде автоматического создания сделки.', '<p>Задача № 1. Нужно настроить передачу с 1 сайта + 2х лендингов в АМО UTM МЕТКОК&nbsp;</p><p>Для этого нужно:</p><p>&nbsp;1. Настроить на сайте передачу UTM-метки в письмо&nbsp;</p><p>2. В рекламных обьявлениях контекстолог добавляет метку в каждый ключ. В источниках на наш сайт должны быть ссылки с ютм метками допустим для рекламы в вк ссылка такого вида: utm_source=vkontakte&amp;utm_medium=cpc&amp;utm_campaign={campaign_id}&amp;utm_content={ad_id}&nbsp;</p><p>Этот ресурс генерирует метки: https://tilda.cc/ru/utm/&nbsp;</p><p>3. Настроить сохранение ютм меток в кэше браузера, на тот случай если пользователь перейдет на другую страницу сайта и с нее оформит заявку&nbsp;</p><p>4. передача ютм меток в создаваемую сделку в амосрм в поля, которые есть в карточке сделки Что должно отображаться в карточке сделки по UTM-меткам: -utm_source - Источник кампании -utm_medium - Тип трафика -utm_campaign - Название кампании -utm_content - Идентификатор объявления -utm_term - Ключевое слово&nbsp;</p><p>Задача № 2. На сайте и на лендинга 2 шт будут добавлены в шапку новые формы обратной связи и в форме будет возможность прикрепить файл, нужно будет эти формы соединить с АМО, чтобы заявки, отправленные с этих форм попадали в воронку НОВЫЕ ЗАЯВКИ в виде автоматического создания сделки.</p>', NULL, NULL, '0.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:17:02', '2020-03-10 18:17:02', 'https://api.freelancehunt.com/v2/projects/641985', 'https://freelancehunt.com/project/nastroyka-amosrm/641985.html', 'https://api.freelancehunt.com/v2/projects/641985/comments', 'https://api.freelancehunt.com/v2/projects/641985/bids', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(16, 641983, 'project', 'Копирайтинг на английском языке', 'Уважаемые фрилансеры!\nНеобходимо написать тексты на английском языке для коммерческих страниц интернет-магазина.\nПродажа натурального товара для здоровья в Канаде. Магазин работает официально, согласно местного законодательства.\nСейчас нужно 3 текста по 600 слов, если все ок - будет больше.\nПредоставляются источники в виде страниц  сайтов-конкурентов.\n\n\nЕсть требования по SEO. Плотность вхождения основного ключа и обязательное использование 10-30 ключевых слов.\nТребования к копирайтеру:\n- обязательно высокий уровень английского языка!\n- знание SEO\n- ответственное отношение к работе!\n\n\nВ ставках обязательно добавляйте примеры своих работ. Без примеров ставки рассматриваться не будут.\nТакже указывайте стоимость за 100 слов и срок выполнения для текста на 600 слов.\n\n\nЖдем Ваши ставки!', '<p>Уважаемые фрилансеры!</p><p>Необходимо написать тексты на английском языке для коммерческих страниц интернет-магазина.</p><p>Продажа натурального товара для здоровья в Канаде. Магазин работает официально, согласно местного законодательства.</p><p>Сейчас нужно 3 текста по 600 слов, если все ок - будет больше.</p><p>Предоставляются источники в виде страниц &nbsp;сайтов-конкурентов.</p><p><br /></p><p>Есть требования по SEO. Плотность вхождения основного ключа и обязательное использование 10-30 ключевых слов.</p><p>Требования к копирайтеру:</p><p>- обязательно высокий уровень английского языка!</p><p>- знание SEO</p><p>- ответственное отношение к работе!</p><p><br /></p><p>В ставках обязательно добавляйте примеры своих работ. Без примеров ставки рассматриваться не будут.</p><p>Также указывайте стоимость за 100 слов и срок выполнения для текста на 600 слов.</p><p><br /></p><p>Ждем Ваши ставки!</p>', '400.00', 'UAH', '400.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:13:19', '2020-03-10 18:13:19', 'https://api.freelancehunt.com/v2/projects/641983', 'https://freelancehunt.com/project/kopirayting-na-angliyskom-yazyike/641983.html', 'https://api.freelancehunt.com/v2/projects/641983/comments', 'https://api.freelancehunt.com/v2/projects/641983/bids', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(17, 641982, 'project', 'Оживить логотип', 'Есть старый логотип мастера депиляции. Требуеться сделать редизайн (оживить)  в современном минималистичном стиле. Возможно в более строгом. \nВ лого можно добавить текст (мастер-технолог депиляции) \nНа выходе svg, png', '<p>Есть старый логотип мастера депиляции. Требуеться сделать редизайн (оживить) &nbsp;в современном минималистичном стиле. Возможно в более строгом.&nbsp;</p><p>В лого можно добавить текст (мастер-технолог депиляции)&nbsp;</p><p>На выходе svg, png</p><p><br /></p><p><a rel=\"nofollow\" href=\"https://content.freelancehunt.com/textattach/95aae/547e7/653357/IMG_20200303_230619_193.jpg\" style=\"\" target=\"_blank\"><img src=\"https://content.freelancehunt.com/textattach/95aae/547e7/653357/IMG_20200303_230619_193.jpg\" style=\"width: 300px;\" alt=\"image\" /></a></p><p><br /></p>', '1000.00', 'RUB', '380.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:10:55', '2020-03-10 18:10:55', 'https://api.freelancehunt.com/v2/projects/641982', 'https://freelancehunt.com/project/ozhivit-logotip/641982.html', 'https://api.freelancehunt.com/v2/projects/641982/comments', 'https://api.freelancehunt.com/v2/projects/641982/bids', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(18, 641981, 'project', 'Копирайтинге SEO для сайта', 'Нужно написать описание категорий и описание товара, более подробно тут https://docs.google.com/spreadsheets/d/1xyuPQX4JctSZ4RveK7jqQQeY35ibJtMAuPjhHbmf8jw/edit?usp=sharing\nОт вас стоимость за всю работу и сроки.', '<p>Нужно написать описание категорий и описание товара, более подробно тут <a href=\"https://docs.google.com/spreadsheets/d/1xyuPQX4JctSZ4RveK7jqQQeY35ibJtMAuPjhHbmf8jw/edit?usp=sharing\" rel=\"noopener noreferrer nofollow\" target=\"_blank\">https://docs.google.com/spreadsheets/d/1xyuPQX4JctSZ4RveK7jqQQeY35ibJtMAuPjhHbmf8jw/edit?usp=sharing</a></p><p>От вас стоимость за всю работу и сроки. </p>', NULL, NULL, '0.00', 6, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:08:06', '2020-03-10 18:08:06', 'https://api.freelancehunt.com/v2/projects/641981', 'https://freelancehunt.com/project/kopiraytinge-seo-dlya-sayta/641981.html', 'https://api.freelancehunt.com/v2/projects/641981/comments', 'https://api.freelancehunt.com/v2/projects/641981/bids', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(19, 641980, 'project', 'Установить скрипт обьявлений tamagra или любой другой аналог', 'Установить скрипт обьявлений tamagra или любой другой аналог предложение высылайте с примерами \n\n\n\n\n............................\n............................\n............................\n............................\n............................\n............................\n............................', '<p>Установить скрипт обьявлений tamagra или любой другой аналог предложение высылайте с примерами&nbsp;</p><p><br /></p><p><br /></p><p>............................</p><p>............................</p><p>............................</p><p>............................</p><p>............................</p><p>............................</p><p>............................</p>', '500.00', 'UAH', '500.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:07:10', '2020-03-10 18:07:10', 'https://api.freelancehunt.com/v2/projects/641980', 'https://freelancehunt.com/project/ustanovit-skript-obyavleniy-tamagra-ili-lyuboy/641980.html', 'https://api.freelancehunt.com/v2/projects/641980/comments', 'https://api.freelancehunt.com/v2/projects/641980/bids', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(20, 641993, 'project', 'Оптимизация карты для UE4,  создание коллизий для гоночной трассы.', 'В имеющейся карте:\nЗаменить деревья на более реалистичные и сделать их динамичными\nвыставить свет, и тени \nоптимизировать карту , уменьшить полигональность и облегчить карту.\nПо всей гоночной  трассе  исправить колизию.', '<p>В имеющейся карте:</p><p>Заменить деревья на более реалистичные и сделать их динамичными</p><p>выставить свет, и тени&nbsp;</p><p>оптимизировать карту , уменьшить полигональность и облегчить карту.</p><p>По всей гоночной &nbsp;трассе&nbsp; исправить колизию.</p><p><br /></p><p><br /></p>', '2500.00', 'UAH', '2500.00', 0, 0, 0, 0, NULL, 'split', 0, NULL, NULL, '', '2020-03-03 18:30:36', '2020-03-10 18:30:36', 'https://api.freelancehunt.com/v2/projects/641993', 'https://freelancehunt.com/project/optimizatsiya-kartyi-dlya-ue4-sozdanie/641993.html', 'https://api.freelancehunt.com/v2/projects/641993/comments', 'https://api.freelancehunt.com/v2/projects/641993/bids', '2020-03-03 13:33:28', '2020-03-03 13:33:28'),
(21, 641992, 'project', 'Обзвон по ресторанам в США (18:00..24:00, ставка + %)', 'Привет)\nИщем сотрудника кто будет звонить по ресторанам и задавать пару вопросов.\nТипа \"Привет. Мы делаем перетяжку мебели. Интересно?\". Всех кто ответил \"да\" или задал доп вопросы - вносите в базу.\nОплата: ставка и $1 за каждый заинтересованный лид. Ориентируемся на $500..900/в первый месяц. Очень зависит от вашей продуктивности. Первые тесты показали высокую отдачу. Звоните много - получаете много лидов - мы от счастья платим премии.\nГрафик гибкий - можно сдвинуть +/- час. Звонки через нашу систему.', '<p>Привет)<br />Ищем сотрудника кто будет звонить по ресторанам и задавать пару вопросов.</p><p>Типа &quot;Привет. Мы делаем перетяжку мебели. Интересно?&quot;. Всех кто ответил &quot;да&quot; или задал доп вопросы - вносите в базу.</p><p>Оплата: ставка и $1 за каждый заинтересованный лид. Ориентируемся на $500..900/в первый месяц. Очень зависит от вашей продуктивности. Первые тесты показали высокую отдачу. Звоните много - получаете много лидов - мы от счастья платим премии.</p><p>График гибкий - можно сдвинуть +/- час. Звонки через нашу систему.</p>', '12500.00', 'UAH', '12500.00', 0, 1, 0, 0, NULL, NULL, 0, NULL, NULL, '', '2020-03-03 18:30:22', '2020-03-10 18:31:24', 'https://api.freelancehunt.com/v2/projects/641992', 'https://freelancehunt.com/project/obzvon-po-restoranam-ssha-18002400/641992.html', 'https://api.freelancehunt.com/v2/projects/641992/comments', 'https://api.freelancehunt.com/v2/projects/641992/bids', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(22, 641990, 'project', 'Установить скрипт TAMARANGA CITY 3.0 - СКРИПТ ГОРОДСКОГО ПОРТАЛА', 'Установить скрипт TAMARANGA CITY 3.0 - СКРИПТ ГОРОДСКОГО ПОРТАЛА  или другой любой аналог \n..............................\n..............................\n..............................\n..............................\n..............................\n..............................\n..............................\n..............................\n..............................\n..............................', '<p>Установить скрипт TAMARANGA CITY 3.0 - СКРИПТ ГОРОДСКОГО ПОРТАЛА &nbsp;или другой любой аналог&nbsp;</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p><p>..............................</p>', NULL, NULL, '0.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:28:57', '2020-03-10 18:28:57', 'https://api.freelancehunt.com/v2/projects/641990', 'https://freelancehunt.com/project/ustanovit-skript-tamaranga-city-30/641990.html', 'https://api.freelancehunt.com/v2/projects/641990/comments', 'https://api.freelancehunt.com/v2/projects/641990/bids', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(23, 641989, 'project', 'Laravel+vuejs junior developer', 'Я сам девелопеп і шукаю когось в допомогу на різні проекти, стек laravel+vue, бажано зі Львова так як потрібно працювати в офісі.\nГотовий допомагати розвиватись, навчу, покажу і тд. Англ не обовзякова.\nЄдина вимога - відповідальність.', '<p>Я сам девелопеп і шукаю когось в допомогу на різні проекти, стек laravel+vue, бажано зі Львова так як потрібно працювати в офісі.</p><p>Готовий допомагати розвиватись, навчу, покажу і тд. Англ не обовзякова.</p><p>Єдина вимога - відповідальність.</p>', NULL, NULL, '0.00', 1, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:23:37', '2020-03-24 18:26:39', 'https://api.freelancehunt.com/v2/projects/641989', 'https://freelancehunt.com/project/laravelplusvuejs-junior-developer/641989.html', 'https://api.freelancehunt.com/v2/projects/641989/comments', 'https://api.freelancehunt.com/v2/projects/641989/bids', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(24, 641995, 'project', 'Администратор онлайн - магазина', 'Администратор онлайн магазина \n\n\nЗАДАЧИ:\nОтвечать на входящие заявки;\nРазмещать объявления, работать с откликами на них;\nпомогать покупателям оформлять дисконт на сайте\n\n\nТРЕБОВАНИЯ:\n2-3 часа в день онлайн, можно не подряд, режим дня любой.\nпройти бесплатное обучение;\nжелательно быть активным пользователем соцсетей.\n\n\nОПЛАТА: % от созданного товарооборота, от 3 до 30,000 рублей.\nПисать вацап, Вайбер, телеграм 89054573523\nИли переходите по ссылке \nhttp://pro.vtemebiz.ru/?from=vekref5427u352487', '<p>Администратор онлайн магазина&nbsp;</p><p><br /></p><p>ЗАДАЧИ:</p><p>Отвечать на входящие заявки;</p><p>Размещать объявления, работать с откликами на них;</p><p>помогать покупателям оформлять дисконт на сайте</p><p><br /></p><p>ТРЕБОВАНИЯ:</p><p>2-3 часа в день онлайн, можно не подряд, режим дня любой.</p><p>пройти бесплатное обучение;</p><p>желательно быть активным пользователем соцсетей.</p><p><br /></p><p>ОПЛАТА: % от созданного товарооборота, от 3 до 30,000 рублей.</p><p>Писать вацап, Вайбер, телеграм 89054573523</p><p>Или переходите по ссылке&nbsp;</p><p><a href=\"http://pro.vtemebiz.ru/?from=vekref5427u352487\" rel=\"noopener noreferrer nofollow\" target=\"_blank\">http://pro.vtemebiz.ru/?from=vekref5427u352487</a></p>', '25000.00', 'RUB', '9500.00', 0, 1, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:35:48', '2020-03-10 18:35:48', 'https://api.freelancehunt.com/v2/projects/641995', 'https://freelancehunt.com/project/administrator-onlayn-magazina/641995.html', 'https://api.freelancehunt.com/v2/projects/641995/comments', 'https://api.freelancehunt.com/v2/projects/641995/bids', '2020-03-03 13:38:14', '2020-03-03 13:38:14'),
(25, 641994, 'project', 'Нужна помощь', 'Есть сайт branchmobile.ru  нужно помощь с стилем.  Там где написано Различные варианты дизайна и Прочность и Небольшая цена нужно сделать эти столбцы по бокам.\nАналогично с блоком где написано В начале Контактная форма и Подтверждение. тоже нужно боковые в бока а тот что посередине вниз блока сделать.\nНужно сделать так, чтобы видео не перекрывалось текстом. на сайте все сами увидите.\nРабота легкая требуется в css прописать всего несколько строк которые поменяют расположение столбцов.\nФото внизу приложил', '<p>Есть сайт&nbsp;<a href=\"http://branchmobile.ru/\" rel=\"nofollow noopener\" target=\"_blank\">branchmobile.ru</a>&nbsp; нужно помощь с стилем. &nbsp;Там где написано Различные варианты дизайна и Прочность и Небольшая цена нужно сделать эти столбцы по бокам.<br />Аналогично с блоком где написано В начале Контактная форма и Подтверждение. тоже нужно боковые в бока а тот что посередине вниз блока сделать.<br />Нужно сделать так, чтобы видео не перекрывалось текстом. на сайте все сами увидите.<br />Работа легкая требуется в css прописать всего несколько строк которые поменяют расположение столбцов.<br />Фото внизу приложил</p>', NULL, NULL, '0.00', 5, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:33:32', '2020-03-10 18:33:32', 'https://api.freelancehunt.com/v2/projects/641994', 'https://freelancehunt.com/project/nuzhna-pomosch/641994.html', 'https://api.freelancehunt.com/v2/projects/641994/comments', 'https://api.freelancehunt.com/v2/projects/641994/bids', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(26, 641998, 'project', 'Создать 2 анимационных баннера', 'Задача: сделать 2 анимированных баннера для рекламы на веб-ресурсах.\n\n\nБаннеры должны быть с привлекательной анимацией (появление текста + чередование слайдов), эффЕктными, но в то же время эффектИвными.\n\n\nТехнические требования к баннерам:\n1220*100 и вес до 100кб\n\n\nПервый баннер, рекламируем услугу создания Интернет-магазинов, \nЦА - предприниматели, торговые фирмы у которых уже есть или те, которые только думают о выходе в онлайн.\n\n\nПредварительный текст, который нужно обыграть анимационными эффектами чередой слайдов (если текста много или если будет встречное, лучшее предложение, всё обсуждается, т.е. это набросок): \n1 слайд. СОЗДАНИЕ ИНТЕРНЕТ-МАГАЗИНОВ С НУЛЯ И \"ПОД КЛЮЧ\"\n2 слайд. 7 ЛЕТ ОПЫТА. ВЫПОЛНЯЕМ ЛЮБЫЕ ЗАДАЧИ.\n3 слайд. ВЕБ-СТУДИЯ NEOSEO: ПРОПИСАНО - СДЕЛАНО.\n4 слайд. \"ВДРУГ\" не умирает БАБУШКА ... - все проекты сдаются в сроки (тут можно показать подмигивающую бабушку)\n\n\nИ второй баннер, рекламируем модули / дополнения для веб-мастеров, которые работают с OpenCart\n\n\n1 слайд - Хочешь создавать Интернет-магазины быстро и зарабатывать больше?!\n2 слайд - У нас есть 100+ модулей для CMS OpenCart\n3 слайд - Регистрируйся как ВЕБ-МАСТЕР и Получай до 90% скидки\n\n\n\n\n\n\n ', '<p>Задача: сделать 2 анимированных баннера для рекламы на веб-ресурсах.</p><p><br /></p><p>Баннеры должны быть с привлекательной анимацией (появление текста + чередование слайдов), эффЕктными, но в то же время эффектИвными.</p><p><br /></p><p>Технические требования к баннерам:</p><p>1220*100 и вес до 100кб</p><p><br /></p><p>Первый баннер, рекламируем услугу создания Интернет-магазинов,&nbsp;</p><p>ЦА - предприниматели, торговые фирмы у которых уже есть или те, которые только думают о выходе в онлайн.</p><p><br /></p><p>Предварительный текст, который нужно обыграть анимационными эффектами чередой слайдов (если текста много или если будет встречное, лучшее предложение, всё обсуждается, т.е. это набросок):&nbsp;</p><p>1 слайд. СОЗДАНИЕ ИНТЕРНЕТ-МАГАЗИНОВ С НУЛЯ И &quot;ПОД КЛЮЧ&quot;</p><p>2 слайд. 7 ЛЕТ ОПЫТА. ВЫПОЛНЯЕМ ЛЮБЫЕ ЗАДАЧИ.</p><p>3 слайд. ВЕБ-СТУДИЯ NEOSEO: ПРОПИСАНО - СДЕЛАНО.</p><p>4 слайд. &quot;ВДРУГ&quot; не умирает БАБУШКА ... - все проекты сдаются в сроки (тут можно показать подмигивающую бабушку)</p><p><br /></p><p>И второй баннер, рекламируем модули / дополнения для веб-мастеров, которые работают с OpenCart</p><p><br /></p><p>1 слайд - Хочешь создавать Интернет-магазины быстро и зарабатывать больше?!</p><p>2 слайд - У нас есть 100+ модулей для CMS OpenCart</p><p>3 слайд - Регистрируйся как ВЕБ-МАСТЕР и Получай до 90% скидки</p><p><br /></p><p><br /></p><p><br /></p><p>&nbsp;</p>', '2500.00', 'UAH', '2500.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:45:56', '2020-03-10 18:45:56', 'https://api.freelancehunt.com/v2/projects/641998', 'https://freelancehunt.com/project/sozdat-animatsionnyih-bannera/641998.html', 'https://api.freelancehunt.com/v2/projects/641998/comments', 'https://api.freelancehunt.com/v2/projects/641998/bids', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(27, 641997, 'project', 'Необходимо создать заставку', 'Необходимо создать заставку для видео на YouTube. \nПримеры, которые нам нравятся:\n https://youtu.be/OsnwkxadQro \nhttps://youtu.be/CqzlZgC6OUU \nhttps://youtu.be/AzQqtJYOvmc \nhttps://youtu.be/BFmG5t650M4', '<p>Необходимо создать заставку для видео на YouTube.&nbsp;</p><p>Примеры, которые нам нравятся:</p><p>&nbsp;<a rel=\"nofollow\" href=\"https://client.work-zilla.com/away?url=https%3A%2F%2Fyoutu.be%2FOsnwkxadQro\" target=\"_blank\">https://youtu.be/OsnwkxadQro</a>&nbsp;</p><p><a rel=\"nofollow\" href=\"https://client.work-zilla.com/away?url=https%3A%2F%2Fyoutu.be%2FCqzlZgC6OUU\" target=\"_blank\">https://youtu.be/CqzlZgC6OUU</a>&nbsp;</p><p><a rel=\"nofollow\" href=\"https://client.work-zilla.com/away?url=https%3A%2F%2Fyoutu.be%2FAzQqtJYOvmc\" target=\"_blank\">https://youtu.be/AzQqtJYOvmc</a>&nbsp;</p><p><a rel=\"nofollow\" href=\"https://client.work-zilla.com/away?url=https%3A%2F%2Fyoutu.be%2FBFmG5t650M4\" target=\"_blank\">https://youtu.be/BFmG5t650M4</a> </p>', NULL, NULL, '0.00', 0, 0, 0, 0, NULL, 'split', 0, NULL, NULL, '', '2020-03-03 18:45:47', '2020-03-10 18:45:47', 'https://api.freelancehunt.com/v2/projects/641997', 'https://freelancehunt.com/project/neobhodimo-sozdat-zastavku/641997.html', 'https://api.freelancehunt.com/v2/projects/641997/comments', 'https://api.freelancehunt.com/v2/projects/641997/bids', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(28, 641996, 'project', 'Добрый день', 'Надо сделать сайт.\nДизайн есть. Тоесть фотки, как он должен выглядить.\nБолее подробное техническое задание скину исполнителю.\nТогда уже обговорим цену и сроки.\nСайт каталог мебели, карточка товара, фильтры, блокнот.', '<p>Надо сделать сайт.</p><p>Дизайн есть. Тоесть фотки, как он должен выглядить.</p><p>Более подробное техническое задание скину исполнителю.</p><p>Тогда уже обговорим цену и сроки.<br />Сайт каталог мебели, карточка товара, фильтры, блокнот.</p><p><br /></p>', NULL, NULL, '0.00', 7, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:38:28', '2020-03-10 18:38:28', 'https://api.freelancehunt.com/v2/projects/641996', 'https://freelancehunt.com/project/dobryiy-den/641996.html', 'https://api.freelancehunt.com/v2/projects/641996/comments', 'https://api.freelancehunt.com/v2/projects/641996/bids', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(29, 642013, 'project', 'Доработка Android приложения', 'У нас приложение \"Правое полушарие Интроверта\"\nЗадача приложения продавать билеты на лекции об искусстве и предоставлять контент по подписке\n\nНеобходимо доработать, а именно:\n- Добавить функцию \"Добавить в избранное\" аудиолекцию\n- Установить модуль от яндекс кассы Google Pay в приложение\n- Модернизировать плеер. Сделать по аналогии с приложением \"Арзамас\"(запоминание с какого момента выключили приложение,\nФоновое прослушивание)\n- Добавить функцию просмотра pdf файлов(презентации и конспектов лекций) в разделе мои билеты\n- Пофиксить баги\n- Небольшие мелкие правки\n\nДизайн макеты приложения есть.\nДизайнер проверить и поможет реализовать задуманное.\nВ качестве ориентира у нас есть версия на iOS, где практически всё готово.\n\nСумма - 25 000\nСрок - 4 дня\nНеобходимо плотно сесть и сделать всё сразу.', '<p>У нас приложение &quot;Правое полушарие Интроверта&quot;<br />Задача приложения продавать билеты на лекции об искусстве и предоставлять контент по подписке<br /><br />Необходимо доработать, а именно:<br />- Добавить функцию &quot;Добавить в избранное&quot; аудиолекцию<br />- Установить модуль от яндекс кассы Google Pay в приложение<br />- Модернизировать плеер. Сделать по аналогии с приложением &quot;Арзамас&quot;(запоминание с какого момента выключили приложение,<br />Фоновое прослушивание)<br />- Добавить функцию просмотра pdf файлов(презентации и конспектов лекций) в разделе мои билеты<br />- Пофиксить баги<br />- Небольшие мелкие правки<br /><br />Дизайн макеты приложения есть.<br />Дизайнер проверить и поможет реализовать задуманное.<br />В качестве ориентира у нас есть версия на iOS, где практически всё готово.<br /><br />Сумма - 25 000<br />Срок - 4 дня<br />Необходимо плотно сесть и сделать всё сразу. </p>', '25000.00', 'RUB', '9500.00', 0, 0, 0, 0, NULL, 'split', 0, NULL, NULL, '', '2020-03-03 19:09:57', '2020-03-10 19:09:57', 'https://api.freelancehunt.com/v2/projects/642013', 'https://freelancehunt.com/project/dorabotka-android-prilozheniya/642013.html', 'https://api.freelancehunt.com/v2/projects/642013/comments', 'https://api.freelancehunt.com/v2/projects/642013/bids', '2020-03-03 14:13:52', '2020-03-03 14:13:52'),
(30, 642011, 'project', 'Mini Go / Golang Project - check condition, provide config UI then app', 'Create a mini app to provide a WiFi configuration menu over WiFi when no ethernet or WiFi connection is detected.\n\nSomething like this:\nhttps://github.com/sabhiram/raspberry-wifi-conf\n\nA cut down version of this template would be fine for the UI:\nhttps://github.com/BlackrockDigital/startbootstrap-sb-admin-2\n\nAuthentication should be via a single username (admin) and a custom password set in a config file.\n\nCreate a readme file showing how to run it.', '<p>Create a mini app to provide a WiFi configuration menu over WiFi when no ethernet or WiFi connection is detected.<br /><br />Something like this:<br /><a rel=\"nofollow\" href=\"https://github.com/sabhiram/raspberry-wifi-conf\" target=\"_blank\">https://github.com/sabhiram/raspberry-wifi-conf</a><br /><br />A cut down version of this template would be fine for the UI:<br /><a rel=\"nofollow\" href=\"https://github.com/BlackrockDigital/startbootstrap-sb-admin-2\" target=\"_blank\">https://github.com/BlackrockDigital/startbootstrap-sb-admin-2</a><br /><br />Authentication should be via a single username (admin) and a custom password set in a config file.<br /><br />Create a readme file showing how to run it.</p>', NULL, NULL, '0.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 19:03:35', '2020-03-05 19:03:35', 'https://api.freelancehunt.com/v2/projects/642011', 'https://freelancehunt.com/project/mini-golang-project/642011.html', 'https://api.freelancehunt.com/v2/projects/642011/comments', 'https://api.freelancehunt.com/v2/projects/642011/bids', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(31, 642010, 'project', 'Поиск спонсоров на Giveaway', 'Ищу людей которые опытные в этом деле, знают где и кого искать\nНужно найти 35 спонсоров, текст вышлю\nНапишу сразу все условия, дабы вам сразу была ясна картина работы:\n1. Нужно будет искать спонсоров на гив, а так же общаться с ними, скидывать им информацию о гиве, о стоимости участия и тд. Всё что нужно писать я скину. И как только вы находите того кто согласен стать спонсором и внести оплату - вы мне скидываете его контакт.\n2. Спонсор оплачивает участие в гиве и я вам сразу же скидываю оплату на карту за найденного спонсора.\n3. Все переписки со спонсорами до момента их согласия ведёте вы и это означает что вы будете знать что человек согласился и вы за него должны получить оплату. Мне не нужно чтобы мне скинули много контактов тех кто не будет согласен. Мне нужны контакты тех кто уже согласился. За это вы и получаете оплату. За то что находите этих людей.\n4. Подытожу: один спонсор согласился - скинули мне контакт - он оплатил - вы получили оплату. Оплата за одного спонсора 300 грн', '<p>Ищу людей которые опытные в этом деле, знают где и кого искать</p><p>Нужно найти 35 спонсоров, текст вышлю</p><p>Напишу сразу все условия, дабы вам сразу была ясна картина работы:</p><p>1. Нужно будет искать спонсоров на гив, а так же общаться с ними, скидывать им информацию о гиве, о стоимости участия и тд. Всё что нужно писать я скину. И как только вы находите того кто согласен стать спонсором и внести оплату - вы мне скидываете его контакт.<br />2. Спонсор оплачивает участие в гиве и я вам сразу же скидываю оплату на карту за найденного спонсора.<br />3. Все переписки со спонсорами до момента их согласия ведёте вы и это означает что вы будете знать что человек согласился и вы за него должны получить оплату. Мне не нужно чтобы мне скинули много контактов тех кто не будет согласен. Мне нужны контакты тех кто уже согласился. За это вы и получаете оплату. За то что находите этих людей.<br />4. Подытожу: один спонсор согласился - скинули мне контакт - он оплатил - вы получили оплату. Оплата за одного спонсора 300 грн</p>', NULL, NULL, '0.00', 0, 0, 0, 0, NULL, 'split', 0, NULL, NULL, '', '2020-03-03 19:00:18', '2020-03-07 19:00:18', 'https://api.freelancehunt.com/v2/projects/642010', 'https://freelancehunt.com/project/poisk-sponsorov-na-giveaway/642010.html', 'https://api.freelancehunt.com/v2/projects/642010/comments', 'https://api.freelancehunt.com/v2/projects/642010/bids', '2020-03-03 14:13:53', '2020-03-03 14:13:53');
INSERT INTO `frlh_projects` (`id`, `p_id`, `p_type`, `name`, `description`, `description_html`, `budget_amount`, `budget_currency`, `budget_uah`, `bid_count`, `is_remote_job`, `is_premium`, `is_only_for_plus`, `location`, `safe_type`, `is_personal`, `freelancer`, `p_tags`, `p_updates`, `published_at`, `expired_at`, `links_self_api`, `links_self_web`, `links_comments`, `links_bids`, `created_at`, `updated_at`) VALUES
(32, 642009, 'project', 'Копирайтер (постоянное сотрудничество)', 'Ищу надёжного копирайтера, который пишет качественно - в первую очередь для людей, а потом для сео (с \"ключами\"). Для взаимовыгодного сотрудничества. \nНаписание таких видов статей:\n1. Информативные тексты, полезные для людей. \n2. Продающие статьи, уникальные с ключами. \nРассмотрю все заявки из любого города Украины. \n\n\nИнтересует только надёжный человек, который периодически будет на связи, соблюдает сроки и делает нормальную работу. \nВ ставке или личным сообщением прошу озвучить вашу приемлемую цену при потоке заказов за тексты стабильного хорошего качества. ', '<p>Ищу надё<span style=\"background-color: transparent;\">жного копирайтера, который пишет качественно - в первую очередь для людей, а потом для сео (с &quot;ключами&quot;). </span><span style=\"background-color: transparent;\">Для взаимовыгодного сотрудничества.&nbsp;</span></p><p><span style=\"background-color: transparent;\">Написание таких видов статей:</span></p><p><span style=\"background-color: transparent;\">1. Информативные тексты, полезные для людей.&nbsp;</span></p><p><span style=\"background-color: transparent;\">2. Продающие статьи, уникальные с ключами.&nbsp;</span></p><p><span style=\"background-color: transparent;\">Рассмотрю все заявки из любого города Украины.&nbsp;</span></p><p><br /></p><p><span style=\"background-color: transparent;\">Интересует только надёжный человек, который периодически будет на связи, соблюдает сроки и делает нормальную работу.&nbsp;</span></p><p><span style=\"background-color: transparent;\">В ставке или личным сообщением прошу озвучить вашу приемлемую цену при потоке заказов за тексты стабильного хорошего качества.&nbsp;</span></p><p><br /></p>', NULL, NULL, '0.00', 20, 1, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:58:50', '2020-03-23 18:58:50', 'https://api.freelancehunt.com/v2/projects/642009', 'https://freelancehunt.com/project/kopirayter-postoyannoe-sotrudnichestvo/642009.html', 'https://api.freelancehunt.com/v2/projects/642009/comments', 'https://api.freelancehunt.com/v2/projects/642009/bids', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(33, 642007, 'project', 'Оптимизация под Google PageSpeed', 'Сократите размер структуры DOM  , удалите неиспользуемый код CSS , устранить ресурсы блокирующие отображение и ты.ды\nТест Google PageSpeed    https://developers.google.com/speed/pagespeed/insights/?hl=RU&url=http%3A%2F%2Fsexd.club%2F&tab=desktop ', '<p>Сократите размер структуры DOM &nbsp;, удалите неиспользуемый код CSS , устранить ресурсы блокирующие отображение и ты.ды</p><p>Тест Google PageSpeed &nbsp; &nbsp;<a rel=\"nofollow\" href=\"https://developers.google.com/speed/pagespeed/insights/?hl=RU&amp;url=http%3A%2F%2Fsexd.club%2F&amp;tab=desktop\" target=\"_blank\">https://developers.google.com/speed/pagespeed/insights/?hl=RU&amp;url=http%3A%2F%2Fsexd.club%2F&amp;tab=desktop</a>&nbsp;</p>', NULL, NULL, '0.00', 6, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:57:57', '2020-03-10 18:57:57', 'https://api.freelancehunt.com/v2/projects/642007', 'https://freelancehunt.com/project/optimizatsiya-pod-google-pagespeed/642007.html', 'https://api.freelancehunt.com/v2/projects/642007/comments', 'https://api.freelancehunt.com/v2/projects/642007/bids', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(34, 642008, 'project', 'Доделать сайт на Ruby on Rails', 'Есть сайт с базой данных и парсером, все написано на Ruby on Rails. Требуется обновить некоторые линки и восстановить работу парсера - не поддерживалась год. Потребуется несколько часов работы. Цена договорная. Если все получится, будет еще один проект', '<p>Есть сайт с базой данных и парсером, все написано на Ruby on Rails. Требуется обновить некоторые линки и восстановить работу парсера - не поддерживалась год. Потребуется несколько часов работы. Цена договорная. Если все получится, будет еще один проект</p>', '5000.00', 'RUB', '1900.00', 0, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:57:57', '2020-03-10 18:57:57', 'https://api.freelancehunt.com/v2/projects/642008', 'https://freelancehunt.com/project/dodelat-sayt-na-ruby-rails/642008.html', 'https://api.freelancehunt.com/v2/projects/642008/comments', 'https://api.freelancehunt.com/v2/projects/642008/bids', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(35, 642006, 'project', 'Переписать сайт', 'Нужно полностью сайт bestleasing.com.ua Данный сайт написан на WordPress с использованием Плагина Elementor. Использования данного плагина значительно повеяло на скорость сайта как для моб так и для ПК. Разные плагины кэширования не помогают. Пришли к тому что бы переписать сайт, и ускорить работу сайта.\nВизуально сам сайт не меняться.', '<p>Нужно полностью сайт <a href=\"//bestleasing.com\" rel=\"noopener noreferrer nofollow\" target=\"_blank\">bestleasing.com</a>.ua Данный сайт написан на WordPress с использованием Плагина Elementor. Использования данного плагина значительно повеяло на скорость сайта как для моб так и для ПК. Разные плагины кэширования не помогают. Пришли к тому что бы переписать сайт, и ускорить работу сайта.</p><p>Визуально сам сайт не меняться.</p>', NULL, NULL, '0.00', 0, 0, 1, 0, NULL, 'employer_cashless', 0, NULL, NULL, '', '2020-03-03 18:55:11', '2020-03-10 18:55:10', 'https://api.freelancehunt.com/v2/projects/642006', 'https://freelancehunt.com/project/perepisat-sayt/642006.html', 'https://api.freelancehunt.com/v2/projects/642006/comments', 'https://api.freelancehunt.com/v2/projects/642006/bids', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(36, 642005, 'project', 'Разработчик для PrestaShop', 'Доброго времени суток.\n\nНужен разработчик для проектов на CMS PrestShop.\n\nОбязательно напишите:\n1. Ваш часовой рейт.\n2. Ссылки на выполненные работы с PrestaShop.\n3. Сколько часов в неделю вам было комфортно работать.\n\nСпасибо)', '<p>Доброго времени суток.<br /><br />Нужен разработчик для проектов на CMS PrestShop.<br /><br />Обязательно напишите:<br />1. Ваш часовой рейт.<br />2. Ссылки на выполненные работы с PrestaShop.<br />3. Сколько часов в неделю вам было комфортно работать.<br /><br />Спасибо)</p>', NULL, NULL, '0.00', 1, 1, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:54:39', '2020-03-10 18:54:39', 'https://api.freelancehunt.com/v2/projects/642005', 'https://freelancehunt.com/project/razrabotchik-dlya-prestashop/642005.html', 'https://api.freelancehunt.com/v2/projects/642005/comments', 'https://api.freelancehunt.com/v2/projects/642005/bids', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(37, 642004, 'project', 'Дизайн каталога производителя детских товаров.', 'Смотрю отклики с подобными работами. \n\n\nСделать дизайн каталога производителя детских товаров.\n\n\nПодробное тз по ссылке https://docs.google.com/document/d/11swvEUBLDn6hwBkrjZE1EumRXxREE-gmy9nR4ghGsOc/\n\n\nВ папке где лежит этот файл весь контент для каталога.\n\n\nСрок со всеми правками чт до 10-00\nСвязь желайтельно в вайбер.\n\n\nПоложил несколько примером, которые кажутся неплохими, даже наброски в файле есть.\n\n\n3 первых страницы жду завтра до 12, и двигаемся дальше с корректировками. Можно по одной, даже лучше чтобы со стилистикой разобраться в начале. ', '<p>Смотрю отклики с подобными работами.&nbsp;</p><p><br /></p><p>Сделать дизайн каталога производителя детских товаров.</p><p><br /></p><p>Подробное тз по ссылке https://docs.google.com/document/d/11swvEUBLDn6hwBkrjZE1EumRXxREE-gmy9nR4ghGsOc/</p><p><br /></p><p>В папке где лежит этот файл весь контент для каталога.</p><p><br /></p><p>Срок со всеми правками чт до 10-00</p><p>Связь желайтельно в вайбер.</p><p><br /></p><p>Положил несколько примером, которые кажутся неплохими, даже наброски в файле есть.</p><p><br /></p><p>3 первых страницы жду завтра до 12, и двигаемся дальше с корректировками. Можно по одной, даже лучше чтобы со стилистикой разобраться в начале.&nbsp;</p>', NULL, NULL, '0.00', 4, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:53:22', '2020-03-04 18:53:22', 'https://api.freelancehunt.com/v2/projects/642004', 'https://freelancehunt.com/project/dizayn-kataloga-proizvoditelya-detskih-tovarov/642004.html', 'https://api.freelancehunt.com/v2/projects/642004/comments', 'https://api.freelancehunt.com/v2/projects/642004/bids', '2020-03-03 14:13:55', '2020-03-03 14:13:55'),
(38, 642003, 'project', 'Верстка программирование для сайта адвокатского бюро (3 языка)', 'нужно сверстать и поставить на cms сайт. Сайт будет в 3 языковых версиях. \nДизайн разработаем, предоставим. Тематика: услуги адвоката (7 стр с описанием услуг , стр с видео и статьями ,  продающая главная страница с формами и обратным звонком, стр контактов). Сайт должен быть удоьен заказчику для редактирования страниц. \nТакже должна быть возможность управления  контентом для сео (мета теги, контент, заголовки) и легкость индексации сайта на выбранном вами движке. \nПредложите плз cms + оцените плз вашу работу  + покажите пожалуйста ваше портфолио . \nПример похожего по контенту сайта: https://www.abuwarda.co.il/ (у нас будет более современный дизайн)', '<p>нужно сверстать и поставить на cms сайт. Сайт будет в 3 языковых версиях.&nbsp;</p><p>Дизайн разработаем, предоставим. Тематика: услуги адвоката (7 стр с описанием услуг , стр с видео и статьями , &nbsp;продающая главная страница с формами и обратным звонком, стр контактов). Сайт должен быть удоьен заказчику для редактирования страниц.&nbsp;</p><p>Также должна быть возможность управления &nbsp;контентом для сео (мета теги, контент, заголовки) и легкость индексации сайта на выбранном вами движке.&nbsp;</p><p>Предложите плз cms + оцените плз вашу работу&nbsp; + покажите пожалуйста ваше портфолио .&nbsp;</p><p>Пример похожего по контенту сайта:&nbsp;<a href=\"https://www.abuwarda.co.il/\" rel=\"noopener noreferrer nofollow\" target=\"_blank\">https://www.abuwarda.co.il/</a>&nbsp;(у нас будет более современный дизайн)</p>', NULL, NULL, '0.00', 9, 0, 0, 0, NULL, 'employer', 0, NULL, NULL, '', '2020-03-03 18:52:12', '2020-03-10 18:52:12', 'https://api.freelancehunt.com/v2/projects/642003', 'https://freelancehunt.com/project/verstka-programmirovanie-dlya-sayta-advokatskogo-byuro/642003.html', 'https://api.freelancehunt.com/v2/projects/642003/comments', 'https://api.freelancehunt.com/v2/projects/642003/bids', '2020-03-03 14:13:55', '2020-03-03 14:13:55');

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_skills`
--

CREATE TABLE `frlh_skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_skills`
--

INSERT INTO `frlh_skills` (`id`, `project_id`, `skill_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 99, 'Веб-программирование', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(2, 1, 88, 'Разработка игр', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(3, 2, 116, 'Рефераты, дипломы, курсовые', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(4, 3, 113, 'Аудио/видео монтаж', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(5, 4, 76, 'Копирайтинг', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(6, 5, 124, 'HTML/CSS верстка', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(7, 6, 134, 'SEO-аудит сайтов', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(8, 6, 14, 'Поисковое продвижение (SEO)', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(9, 7, 1, 'PHP', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(10, 7, 99, 'Веб-программирование', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(11, 8, 99, 'Веб-программирование', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(12, 8, 43, 'Дизайн сайтов', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(13, 8, 45, 'Сопровождение сайтов', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(14, 9, 1, 'PHP', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(15, 10, 99, 'Веб-программирование', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(16, 11, 1, 'PHP', '2020-03-03 13:06:59', '2020-03-03 13:06:59'),
(17, 11, 99, 'Веб-программирование', '2020-03-03 13:06:59', '2020-03-03 13:06:59'),
(18, 11, 78, 'Установка и настройка CMS', '2020-03-03 13:06:59', '2020-03-03 13:06:59'),
(19, 12, 96, 'Создание сайта под ключ', '2020-03-03 13:23:18', '2020-03-03 13:23:18'),
(20, 13, 41, 'Баннеры', '2020-03-03 13:23:18', '2020-03-03 13:23:18'),
(21, 13, 17, 'Логотипы', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(22, 14, 43, 'Дизайн сайтов', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(23, 14, 17, 'Логотипы', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(24, 15, 1, 'PHP', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(25, 16, 79, 'Английский язык', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(26, 16, 76, 'Копирайтинг', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(27, 16, 125, 'Рерайтинг', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(28, 17, 58, 'Векторная графика', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(29, 18, 76, 'Копирайтинг', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(30, 18, 125, 'Рерайтинг', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(31, 19, 1, 'PHP', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(32, 19, 99, 'Веб-программирование', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(33, 19, 78, 'Установка и настройка CMS', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(34, 20, 88, 'Разработка игр', '2020-03-03 13:33:28', '2020-03-03 13:33:28'),
(35, 21, 171, 'Работа с клиентами', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(36, 21, 79, 'Английский язык', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(37, 22, 1, 'PHP', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(38, 22, 99, 'Веб-программирование', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(39, 22, 78, 'Установка и настройка CMS', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(40, 23, 1, 'PHP', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(41, 24, 138, 'Публикация объявлений', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(42, 25, 124, 'HTML/CSS верстка', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(43, 26, 41, 'Баннеры', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(44, 26, 91, 'Анимация', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(45, 27, 111, 'Визуализация и моделирование', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(46, 27, 113, 'Аудио/видео монтаж', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(47, 28, 96, 'Создание сайта под ключ', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(48, 29, 121, 'Разработка под Android', '2020-03-03 14:13:52', '2020-03-03 14:13:52'),
(49, 30, 174, 'Node.js', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(50, 30, 99, 'Веб-программирование', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(51, 31, 104, 'Контент-менеджер', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(52, 31, 171, 'Работа с клиентами', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(53, 31, 131, 'Продвижение в социальных сетях (SMM)', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(54, 32, 76, 'Копирайтинг', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(55, 32, 38, 'Написание статей', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(56, 32, 125, 'Рерайтинг', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(57, 33, 28, 'Javascript', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(58, 33, 124, 'HTML/CSS верстка', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(59, 34, 23, 'Ruby', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(60, 35, 1, 'PHP', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(61, 35, 124, 'HTML/CSS верстка', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(62, 35, 96, 'Создание сайта под ключ', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(63, 36, 99, 'Веб-программирование', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(64, 37, 75, 'Полиграфический дизайн', '2020-03-03 14:13:55', '2020-03-03 14:13:55'),
(65, 38, 99, 'Веб-программирование', '2020-03-03 14:13:55', '2020-03-03 14:13:55');

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_statuses`
--

CREATE TABLE `frlh_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_statuses`
--

INSERT INTO `frlh_statuses` (`id`, `project_id`, `status_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 11, 'Прием ставок', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(2, 2, 11, 'Прием ставок', '2020-03-03 13:00:28', '2020-03-03 13:00:28'),
(3, 3, 11, 'Прием ставок', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(4, 4, 11, 'Прием ставок', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(5, 5, 11, 'Прием ставок', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(6, 6, 11, 'Прием ставок', '2020-03-03 13:00:29', '2020-03-03 13:00:29'),
(7, 7, 11, 'Прием ставок', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(8, 8, 11, 'Прием ставок', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(9, 9, 11, 'Прием ставок', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(10, 10, 11, 'Прием ставок', '2020-03-03 13:00:30', '2020-03-03 13:00:30'),
(11, 11, 11, 'Прием ставок', '2020-03-03 13:06:59', '2020-03-03 13:06:59'),
(12, 12, 11, 'Прием ставок', '2020-03-03 13:23:18', '2020-03-03 13:23:18'),
(13, 13, 11, 'Прием ставок', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(14, 14, 11, 'Прием ставок', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(15, 15, 11, 'Прием ставок', '2020-03-03 13:23:19', '2020-03-03 13:23:19'),
(16, 16, 11, 'Прием ставок', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(17, 17, 11, 'Прием ставок', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(18, 18, 11, 'Прием ставок', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(19, 19, 11, 'Прием ставок', '2020-03-03 13:23:20', '2020-03-03 13:23:20'),
(20, 20, 11, 'Прием ставок', '2020-03-03 13:33:28', '2020-03-03 13:33:28'),
(21, 21, 11, 'Прием ставок', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(22, 22, 11, 'Прием ставок', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(23, 23, 11, 'Прием ставок', '2020-03-03 13:33:29', '2020-03-03 13:33:29'),
(24, 24, 11, 'Прием ставок', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(25, 25, 11, 'Прием ставок', '2020-03-03 13:38:15', '2020-03-03 13:38:15'),
(26, 26, 11, 'Прием ставок', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(27, 27, 11, 'Прием ставок', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(28, 28, 11, 'Прием ставок', '2020-03-03 13:46:56', '2020-03-03 13:46:56'),
(29, 29, 11, 'Прием ставок', '2020-03-03 14:13:52', '2020-03-03 14:13:52'),
(30, 30, 11, 'Прием ставок', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(31, 31, 11, 'Прием ставок', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(32, 32, 11, 'Прием ставок', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(33, 33, 11, 'Прием ставок', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(34, 34, 11, 'Прием ставок', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(35, 35, 11, 'Прием ставок', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(36, 36, 11, 'Прием ставок', '2020-03-03 14:13:54', '2020-03-03 14:13:54'),
(37, 37, 11, 'Прием ставок', '2020-03-03 14:13:55', '2020-03-03 14:13:55'),
(38, 38, 11, 'Прием ставок', '2020-03-03 14:13:55', '2020-03-03 14:13:55');

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_tags`
--

CREATE TABLE `frlh_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `frlh_tags`
--

INSERT INTO `frlh_tags` (`id`, `project_id`, `tag_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 20, 283650, 'ue4', '2020-03-03 13:33:28', '2020-03-03 13:33:28'),
(2, 29, 2720, 'Android', '2020-03-03 14:13:53', '2020-03-03 14:13:53'),
(3, 29, 225593, 'приложение для android', '2020-03-03 14:13:53', '2020-03-03 14:13:53');

-- --------------------------------------------------------

--
-- Структура таблицы `frlh_users`
--

CREATE TABLE `frlh_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `frlh_employers`
--
ALTER TABLE `frlh_employers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_employer_projects`
--
ALTER TABLE `frlh_employer_projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_failed_jobs`
--
ALTER TABLE `frlh_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_migrations`
--
ALTER TABLE `frlh_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_password_resets`
--
ALTER TABLE `frlh_password_resets`
  ADD KEY `frlh_password_resets_email_index` (`email`);

--
-- Индексы таблицы `frlh_projects`
--
ALTER TABLE `frlh_projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_skills`
--
ALTER TABLE `frlh_skills`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_statuses`
--
ALTER TABLE `frlh_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_tags`
--
ALTER TABLE `frlh_tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `frlh_users`
--
ALTER TABLE `frlh_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frlh_users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `frlh_employers`
--
ALTER TABLE `frlh_employers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `frlh_employer_projects`
--
ALTER TABLE `frlh_employer_projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `frlh_failed_jobs`
--
ALTER TABLE `frlh_failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `frlh_migrations`
--
ALTER TABLE `frlh_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `frlh_projects`
--
ALTER TABLE `frlh_projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `frlh_skills`
--
ALTER TABLE `frlh_skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT для таблицы `frlh_statuses`
--
ALTER TABLE `frlh_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `frlh_tags`
--
ALTER TABLE `frlh_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `frlh_users`
--
ALTER TABLE `frlh_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
