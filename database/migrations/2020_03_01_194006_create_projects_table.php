<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('p_id');
            $table->string('p_type', 100)->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('description_html')->nullable();
            $table->decimal('budget_amount', 12,2)->nullable();
            $table->string('budget_currency', 5)->nullable();
            $table->decimal('budget_uah', 12,2)->nullable();
            $table->integer('bid_count')->nullable();
            $table->tinyInteger('is_remote_job')->nullable();
            $table->tinyInteger('is_premium')->nullable();
            $table->tinyInteger('is_only_for_plus')->nullable();
            $table->string('location')->nullable();
            $table->string('safe_type')->nullable();
            $table->tinyInteger('is_personal')->nullable();
            $table->string('freelancer')->nullable();
            $table->string('updates')->nullable();
            $table->dateTime('published_at')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->string('links_self_api')->nullable();
            $table->string('links_self_web')->nullable();
            $table->string('links_comments')->nullable();
            $table->string('links_bids')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
